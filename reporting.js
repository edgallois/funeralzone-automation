const report = require('multiple-cucumber-html-reporter')
const mv = require('mv')
const fs = require('fs')
const copydir = require('copy-dir')
const rimraf = require('rimraf')
const utility = require('./features/functions/utilityMethods')
require('dotenv').config()

const now = new Date()
const existingResults = process.cwd() + '/archive/FuneralZone_Results_'

const currentTime = now.getHours() + ':' + now.getMinutes()
const currentDate = utility.fullDate()
const opSystem = utility.currentSystem()
let actualSystem
if (opSystem === 'darwin') {
  // Mac
  actualSystem = 'osx'
} else if (opSystem === 'linux') {
  // Linux
  actualSystem = 'linux'
} else if (opSystem === 'win32') {
  // Windows
  actualSystem = 'windows'
}

// If running on macOS, it'll return the latest version (10.14) otherwise it'll return the kernel version of linux
const currentOsVersion = utility.currentSystemVersion()

if (process.env.ENDPOINT_TYPE === 'website') {
  report.generate({
    jsonDir: 'jsonfiles/',
    reportPath: 'results/',
    pageTitle: 'Funeral Zone GUI Test Results - ' + currentDate + ' - ' + currentTime,
    reportName: 'Funeral Zone GUI Test Results - ' + currentDate + ' - ' + currentTime,
    displayDuration: true,
    durationInMS: false,
    openReportInBrowser: false,
    saveCollectedJSON: true,
    disableLog: true,
    metadata: {
      browser: {
        name: process.env.BROWSER,
        version: utility.readFile(process.cwd() + '/browserDetails.js')
      },
      device: process.env.DEVICE,
      platform: {
        name: actualSystem,
        version: currentOsVersion
      }
    },
    customData: {
      title: 'Test run overview:',
      data: [
        { label: 'Project', value: 'Funeral Zone' },
        { label: 'Environment Tested', value: process.env.ENVIRONMENT }
      ]
    }
  })
} else if (process.env.ENDPOINT_TYPE === 'graphQlApi') {
  report.generate({
    jsonDir: 'jsonfiles/',
    reportPath: 'results/',
    pageTitle: 'Funeral Zone API Test Results - ' + currentDate + ' - ' + currentTime,
    reportName: 'Funeral Zone API Test Results - ' + currentDate + ' - ' + currentTime,
    displayDuration: true,
    durationInMS: false,
    openReportInBrowser: false,
    saveCollectedJSON: true,
    disableLog: true,
    metadata: {
      device: process.env.DEVICE,
      platform: {
        name: actualSystem,
        version: currentOsVersion
      }
    },
    customData: {
      title: 'Test run overview',
      data: [
        { label: 'Project', value: 'Funeral Zone' },
        { label: 'Environment', value: process.env.ENVIRONMENT }
      ]
    }
  })
}

// Check to see the results folder in the archive section exists
const archiveFolder = process.cwd() + '/archive/FuneralZone_Results_' + currentDate + '-' + currentTime
if (fs.existsSync(existingResults)) {
  fs.renameSync(existingResults, archiveFolder)
}
const featuresArchive = archiveFolder + '/features/'
const assetsFolder = process.cwd() + '/results/assets/'

/*
* Result archive locations
* We need to move the features and the result into the same folder
* structure, otherwise the embedded detail pages will not load
*/
const resArchive = archiveFolder + '/index.html'
const features = process.cwd() + '/results/features/'
const res = process.cwd() + '/results/index.html'
const assetsArchive = archiveFolder + '/assets/'

// Results
// Checks to see if an index.html report is in the results folder, and moves it to archive
if (fs.existsSync(res)) {
  mv(res, resArchive, function (err) {
    if (err) throw err
  })

  // Copy the entire features folder in results, and put it with the associated results
  copydir.sync(features, featuresArchive)

  // Copy the styling to the results folder
  copydir.sync(assetsFolder, assetsArchive)

  // Remove the features & jsonfiles folders, so it's fresh for the next run
  rimraf.sync(features)
  rimraf.sync(assetsFolder)
}

// Remove the temp file for browser version
const browserFile = process.cwd() + '/browserDetails.js'
if (fs.existsSync(browserFile)) {
  fs.unlinkSync(browserFile)
}

process.exit()
