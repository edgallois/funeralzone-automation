const { Given, When, Then } = require('cucumber')
const assert = require('chai').assert
const search = require('../functions/searchMethods')
const data = require('../functions/dataMethods')

Given('I am on the admin > content > website pages > information pages > {string} page', async function (pageType) {
  let websitePages, informationPages, poem, hymn, expectedUrl, currentUrl, contentMenu
  await search.clickElementCss(this.driver, 'a[class=\'link-admin\']')
  contentMenu = await search.waitForElementCss(this.driver, '.dropdown:nth-of-type(7)')
  await contentMenu.click()

  // Need these web elements to mouse move across the header menu
  websitePages = await search.findElementCss(this.driver, '.dropdown:nth-of-type(7)>ul>li:nth-of-type(1)>a')
  informationPages = await search.findElementCss(this.driver, '.dropdown:nth-of-type(7)>ul>li:nth-of-type(1)>ul>li:nth-of-type(1)>a')
  poem = await search.findElementCss(this.driver, '.dropdown:nth-of-type(7)>ul>li:nth-of-type(1)>ul>li:nth-of-type(1)>ul>li:nth-of-type(2)>a')
  hymn = await search.findElementCss(this.driver, '.dropdown:nth-of-type(7)>ul>li:nth-of-type(1)>ul>li:nth-of-type(1)>ul>li:nth-of-type(3)>a')

  // Move to either poems or hymns based off the scenario requirement
  await this.driver.actions().mouseMove(websitePages).perform()
  await this.driver.sleep(250)
  await this.driver.actions().mouseMove(informationPages).perform()
  if (pageType === 'poems') {
    await poem.click()
    await search.waitUntilUrlContains(this.driver, '/poem/')
    expectedUrl = this.testUrl + '/admin/information/poem/'
    currentUrl = await this.driver.getCurrentUrl()
    assert.strictEqual(currentUrl, expectedUrl, 'The poems url is incorrect or we are on the wrong page')
  } else if (pageType === 'hymns') {
    await hymn.click()
    await search.waitUntilUrlContains(this.driver, '/hymn/')
    expectedUrl = this.testUrl + '/admin/information/hymn/'
    currentUrl = await this.driver.getCurrentUrl()
    assert.strictEqual(currentUrl, expectedUrl, 'The hymns url is incorrect or we are on the wrong page')
  }
})

Given('I select a {string} from the available list', async function (type) {
  let title
  switch (type) {
    case type = 'poem':
      this.featuredPoem = await data.getTextFromElementCss(this.driver, '.table>tbody>tr:nth-of-type(1)>td:nth-of-type(1)')
      await search.clickElementCss(this.driver, '.table>tbody>tr:nth-of-type(1)>td:nth-of-type(2)>a[href*=\'/poem/get\']')
      await search.waitUntilUrlContains(this.driver, '/poem/get')
      title = await data.getAttributeFromElementId(this.driver, 'form_title', 'value')
      assert.strictEqual(this.featuredPoem, title, 'The poem title did not match')
      break
    case type = 'hymn':
      this.featuredHymn = await data.getTextFromElementCss(this.driver, '.table>tbody>tr:nth-of-type(1)>td:nth-of-type(1)')
      await search.clickElementCss(this.driver, '.table>tbody>tr:nth-of-type(1)>td:nth-of-type(2)>a[href*=\'/hymn/get\']')
      await search.waitUntilUrlContains(this.driver, '/hymn/get')
      title = await data.getAttributeFromElementId(this.driver, 'form_title', 'value')
      assert.strictEqual(this.featuredHymn, title, 'The hymn title did not match')
      break
  }
})

Given('I select a previously featured {string} from the list', async function (type) {
  let title
  switch (type) {
    case type = 'poem':
      this.nonFeaturedPoem = await data.getTextFromElementCss(this.driver, '.table>tbody>tr:nth-of-type(1)>td:nth-of-type(1)')
      await search.clickElementCss(this.driver, '.table>tbody>tr:nth-of-type(1)>td:nth-of-type(2)>a[href*=\'/poem/get\']')
      await search.waitUntilUrlContains(this.driver, '/poem/get')
      title = await data.getAttributeFromElementId(this.driver, 'form_title', 'value')
      assert.strictEqual(this.nonFeaturedPoem, title, 'The poem title did not match')
      break
    case type = 'hymn':
      // grab the poem name so we can use it for validation
      this.nonFeaturedHymn = await await data.getTextFromElementCss(this.driver, '.table>tbody>tr:nth-of-type(1)>td:nth-of-type(1)')
      await search.clickElementCss(this.driver, '.table>tbody>tr:nth-of-type(1)>td:nth-of-type(2)>a[href*=\'/hymn/get\']')
      await search.waitUntilUrlContains(this.driver, '/hymn/get')

      // Grab the text from the title and assert it is the same poem
      title = await data.getAttributeFromElementId(this.driver, 'form_title', 'value')
      assert.strictEqual(this.nonFeaturedHymn, title, 'The hymn title did not match')
      break
  }
})

When('I mark the item as {string}', async function (type) {
  if (type === 'featured') {
    // value 1 is 'Yes'
    await search.clickElementCss(this.driver, '#form_is_featured>[value=\'1\']')
  } else if (type === 'not featured') {
    // value 0 is 'No'
    await search.clickElementCss(this.driver, '#form_is_featured>[value=\'0\']')
  }
  await search.clickElementId(this.driver, 'form_submit')
  await search.simpleWaitCss(this.driver, '.container-wide')
  const expectedMessage = 'Successfully saved'
  const actualMessage = await data.getTextFromElementCss(this.driver, '.container-wide>p')
  assert.strictEqual(actualMessage, expectedMessage, 'The save message was not found')
})

Then('The featured {string} is displayed on the page', async function (type) {
  let title
  switch (type) {
    case type = 'poem':
      await this.driver.get(this.testUrl + '/help-resources/arranging-a-funeral/planning-the-service/funeral-poems')
      title = data.getTextFromElementCss(this.driver, '.o-featured-articles-grid>a>p[class*=\'pica\']')
      assert.strictEqual(this.featuredPoem, title, 'The featured poem title was not found')
      break
    case type = 'hymn':
      await this.driver.get(this.testUrl + '/help-resources/arranging-a-funeral/planning-the-service/funeral-hymns')
      title = data.getTextFromElementCss(this.driver, '.o-featured-articles-grid>a>p[class*=\'pica\']')
      assert.equal(this.featuredHymn, title, 'The featured hymn title was not found')
      break
  }
})

Then('The {string} is not featured on the page', async function (type) {
  let exists
  switch (type) {
    case type = 'poem':
      await this.driver.get(this.testUrl + '/help-resources/arranging-a-funeral/planning-the-service/funeral-poems')
      exists = await search.checkItemExistsCss(this.driver, '.o-featured-articles-grid>a>p[class*=\'pica\']')
      if (exists === true) {
        assert.notEqual(this.nonFeaturedPoem, 'The non featured poem was found in error')
      } else {
        assert.isFalse(exists, 'The non featured poem was found in error')
      }
      break
    case type = 'hymn':
      await this.driver.get(this.testUrl + '/help-resources/arranging-a-funeral/planning-the-service/funeral-hymns')
      exists = await search.checkItemExistsCss(this.driver, '.o-featured-articles-grid>a>p[class*=\'pica\']')
      if (exists === true) {
        assert.notEqual(this.nonFeaturedHymn, 'The non featured poem was found in error')
      } else {
        assert.isFalse(exists, 'The non featured poem was found in error')
      }
      break
  }
})
