const { When, Then } = require('cucumber')
const assert = require('chai').assert
const search = require('../functions/searchMethods')
const data = require('../functions/dataMethods')

When('I click on the {string} button on the dashboard', async function (button) {
  switch (button) {
    case button = 'reviews':
      await search.clickElementCss(this.driver, '.pull-left>a[href*=\'account/home/reviews\']')
      break
    case button = 'configure obituaries':
      await search.clickElementCss(this.driver, 'a[href*=\'integrations/branding\']')
      break
    case button = 'receipts':
      await search.clickElementCss(this.driver, 'a[href*=\'company/receipts\']')
      break
    case button = 'email setting':
      await search.clickElementCss(this.driver, 'a[href*=\'company/settings\']')
      break
  }
})

Then('I am taken to the {string} page', async function (pageName) {
  let currentUrl, expectedUrl
  switch (pageName) {
    case pageName = 'reviews':
      await search.simpleWaitCss(this.driver, '.heading--2.kirk>b')
      expectedUrl = this.testUrl + '/account/home/reviews'
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl, expectedUrl, 'The reviews url is incorrect, or we are on the wrong page')
      break
    case pageName = 'branding configuration':
      await search.simpleWaitCss(this.driver, '.row-12>h1')
      expectedUrl = this.testUrl + '/account/integrations/branding'
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl, expectedUrl, 'The obituaries branding url is incorrect, or we are on the wrong page')
      break
    case pageName = 'receipts':
      await search.simpleWaitCss(this.driver, '.push--ends')
      expectedUrl = this.testUrl + '/account/company/receipts'
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl, expectedUrl, 'The receipts url is incorrect, or we are on the wrong page')
      break
    case pageName = 'email settings':
      await search.simpleWaitCss(this.driver, '.row-12.pull-left>h1')
      expectedUrl = this.testUrl + '/account/company/settings'
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl, expectedUrl, 'The email settings url is incorrect, or we are on the wrong page')
      break
  }
})

When('I click the reply button on a review', async function () {
  let replyBtns, i, existed, row
  replyBtns = await search.findElementsCss(this.driver, 'div[class*=\'reply-container\']>a')

  for (i = 0; i <= replyBtns.length; i++) {
    row = i + 1
    existed = await search.checkItemExistsCss(this.driver, 'div[class*=\'reply-container\']>a:nth-of-type(' + row + '')

    if (existed === true) {
      // The button is displayed, so we'll click the reply button
      this.row = row
      await search.clickElementCss(this.driver, 'div[class*=\'reply-container\']>a:nth-of-type(' + this.row + '')
      break
    }
  }
})

When('I click the edit button on a previously left review reply', async function () {
  let replyButtons, i, style, result
  replyButtons = await search.findElementsCss(this.driver, 'div[id*=\'dead_reply\']')

  for (i = 1; i <= replyButtons.length; i++) {
    style = await data.getAttributeFromElementCss(this.driver, '.mini-card.block.shadow--left.fb:nth-of-type(' + i + ')' +
            '>div[class*=\'funeral\']>div[id*=\'dead_reply\']', 'style')
    result = style.includes('display: block')
    if (result === true) {
      this.row = i
      await search.clickElementCss(this.driver, '.mini-card.block.shadow--left.fb:nth-of-type(' + this.row + ')' +
                '>div[class*=\'funeral\']>div[id*=\'dead_reply\']>div>a[class*=\'reply-edit\']')
      break
    }
  }
})

When('I click the delete button on a previously left review reply', async function () {
  let replyButtons, i, style, result
  replyButtons = await search.findElementsCss(this.driver, 'div[id*=\'dead_reply\']')

  for (i = 1; i <= replyButtons.length; i++) {
    style = await data.getAttributeFromElementCss(this.driver, '.mini-card.block.shadow--left.fb:nth-of-type(' + i + ')' +
            '>div[class*=\'funeral\']>div[id*=\'dead_reply\']', 'style')
    result = style.includes('display: block')
    if (result === true) {
      this.row = i
      await search.clickElementCss(this.driver, '.mini-card.block.shadow--left.fb:nth-of-type(' + this.row + ')' +
                '>div[class*=\'funeral\']>div[id*=\'dead_reply\']>div>a[class*=\'btn--ghost grey\']')
      await search.clickElementId(this.driver, 'js-reply-delete')
      break
    }
  }
})

Then('I can leave a reply to a review', async function () {
  let textArea, submitBtn, reply, replyText, message
  textArea = await search.checkItemExistsCss(this.driver, '.reply-to-review:nth-of-type(' + this.row + ')')

  if (textArea === true) {
    await data.enterTextInElementCss(this.driver, '.reply-to-review:nth-of-type(' + this.row + ')', 'This is a test reply')
  } else {
    throw new Error('The review reply text box was not found')
  }

  // Submit the form
  submitBtn = await search.checkItemExistsCss(this.driver, '.fb>span>button[class*=\'btn--action\']:nth-of-type(' + this.row + ')')
  if (submitBtn === true) {
    await search.clickElementCss(this.driver, '.fb>span>button[class*=\'btn--action\']:nth-of-type(' + this.row + ')')
  } else {
    throw new Error('The review submit reply button was not found')
  }

  // check the reply was published
  reply = await search.waitForElementCss(this.driver, '.mini-card.block.shadow--left.fb:nth-of-type(' + this.row + ')' +
        '>div[class*=\'funeral-request\']>div[style*=\'block\']>p[class=\'message\']>b')
  replyText = await reply.getText()
  message = 'Your reply has been posted to the website'
  assert.strictEqual(replyText, message, 'The review reply post message was not found')
})

Then('I can edit the reply to the review', async function () {
  let textArea, submitBtn, reply, replyText, message

  textArea = await search.checkItemExistsCss(this.driver, '.reply-to-review:nth-of-type(' + this.row + ')')
  if (textArea === true) {
    await data.enterTextInElementCss(this.driver, '.reply-to-review:nth-of-type(' + this.row + ')', 'This is an edited reply')
  } else {
    throw new Error('The review reply text box was not found')
  }

  submitBtn = await search.checkItemExistsCss(this.driver, '.fb>span>button[class*=\'btn--action\']:nth-of-type(' + this.row + ')')
  if (submitBtn === true) {
    await search.clickElementCss(this.driver, '.fb>span>button[class*=\'btn--action\']:nth-of-type(' + this.row + ')')
  } else {
    throw new Error('The review submit reply button was not found')
  }

  reply = await search.waitForElementCss(this.driver, '.mini-card.block.shadow--left.fb:nth-of-type(' + this.row + ')' +
        '>div[class*=\'funeral-request\']>div[style*=\'block\']>p:nth-of-type(1)')
  replyText = await reply.getText()
  message = 'Reply: This is an edited reply'
  assert.strictEqual(replyText, message, 'The edited review reply was not found')
})

Then('The review reply is deleted', async function () {
  const confirm = await search.checkItemExistsCss(this.driver, 'p[class*=\'alert--warning\']')
  assert.isTrue(confirm, 'The review reply delete confirmation message was not found')
})
