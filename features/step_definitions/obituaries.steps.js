const { Given, When, Then } = require('cucumber')
const assert = require('chai').assert
const utility = require('../functions/utilityMethods')
const search = require('../functions/searchMethods')
const data = require('../functions/dataMethods')

Given('I am on the create obituary page', async function () {
  await search.clickElementCss(this.driver, 'a[href*=\'/account/services/obituary/new\']')
  await search.simpleWaitCss(this.driver, '#contentMain h1')
  const currentUrl = await this.driver.getCurrentUrl()
  assert.strictEqual(currentUrl, this.testUrl + '/account/services/obituary/new', 'The obituaries url is incorrect, or we are on the incorrect page!')
})

Given('I am on the My Obituaries page', async function () {
  await search.clickElementCss(this.driver, 'a[href*=\'/account/services/obituaries\']')
  await search.waitUntilUrlContains(this.driver, 'services/obituaries')
  const currentUrl = await this.driver.getCurrentUrl()
  const expectedUrl = this.testUrl + '/account/services/obituaries/'
  assert.strictEqual(currentUrl, expectedUrl, 'The obituaries page url is incorrect, or we are on the incorrect page')
})

When('I select an obituary to view', async function () {
  await search.clickElementCss(this.driver, '.obituaries.details>div:nth-of-type(2)>div:nth-of-type(2)>div:nth-of-type(1)>a')
  await this.driver.sleep(300)
  const tabs = await this.driver.getAllWindowHandles()
  await this.driver.switchTo().window(tabs[1])
  await search.simpleWaitCss(this.driver, '.obituary-hero-text>h1')
})

When('I select an obituary to edit and make changes and save', async function () {
  let obituaryDeceased, result, formTitle, formName
  await search.simpleWaitCss(this.driver, '.obituaries.details>div:nth-of-type(2)>h3')
  obituaryDeceased = await data.getTextFromElementCss(this.driver, '.obituaries.details>div:nth-of-type(2)>h3')
  result = obituaryDeceased.includes('Existing Deceased')
  if (result === false) {
    this.currentObitDetail = await data.getTextFromElementCss(this.driver, '.obituaries.details>div:nth-of-type(2)>h3')
    await search.clickElementCss(this.driver, '.obituaries.details>div:nth-of-type(2)>div:nth-of-type(2)>div:nth-of-type(2)>a')
  } else {
    this.currentObitDetail = await data.getTextFromElementCss(this.driver, '.obituaries.details>div:nth-of-type(3)>h3')
    await search.clickElementCss(this.driver, '.obituaries.details>div:nth-of-type(3)>div:nth-of-type(2)>div:nth-of-type(2)>a')
  }

  await search.simpleWaitCss('.row-9.pull-left>div>h1')
  formTitle = await data.getAttributeFromElementId(this.driver, 'form_title', 'value')
  formName = await data.getAttributeFromElementId(this.driver, 'form_last_name', 'value')

  // Edit the names
  await data.enterTextInElementId(this.driver, 'form_title', formTitle + '-edited')
  await data.enterTextInElementId(this.driver, 'form_last_name', formName + '-edited')
  await search.clickElementCss(this.driver, '.js-submit')
})

When('I complete the obituary form without errors', async function () {
  // Client details
  await data.enterTextInElementId(this.driver, 'form_firstname', this.clientFirstName)
  await data.enterTextInElementId(this.driver, 'form_lastname', this.clientLastName)
  await data.enterTextInElementId(this.driver, 'form_phone_number_r', '0747' + utility.getNumberBetweenTwoValues(1000000, 9999999))
  await data.enterTextInElementId(this.driver, 'form_email_r', this.clientEmail)

  // Deceased details
  await data.enterTextInElementId(this.driver, 'form_title', this.deceasedFirstName)
  await data.enterTextInElementId(this.driver, 'form_last_name', this.deceasedLastName)

  // Set date of birth
  await search.clickElementCss(this.driver, '#date-sel-dob-dd>option[value=\'' + utility.getNumberBetweenTwoValues(1, 28) + '\']')
  await search.clickElementCss(this.driver, '#date-sel-dob-mm>option[value=\'' + utility.getNumberBetweenTwoValues(1, 12) + '\']')
  await search.clickElementCss(this.driver, '#date-dob-sel>option[value=\'' + utility.getNumberBetweenTwoValues(1910, 1990) + '\']')

  // Set date of death
  await search.clickElementCss(this.driver, '#date-sel-dod-dd>option[value=\'' + utility.getNumberBetweenTwoValues(1, 28) + '\']')
  await search.clickElementCss(this.driver, '#date-sel-dod-mm>option[value=\'' + utility.getNumberBetweenTwoValues(1, 12) + '\']')
  await search.clickElementCss(this.driver, '#date-dod-sel>option[value=\'2017\']')

  // Funeral service details
  const funeralServiceDropdown = await search.findElementsCss(this.driver, '#autopopulateAddress_f_>option')
  const funeralServiceDropdownLength = funeralServiceDropdown.length

  // Select the item in the dropdown
  await search.clickElementCss(this.driver, '#autopopulateAddress_f_>option:nth-of-type(' + utility.getNumberBetweenTwoValues(1, funeralServiceDropdownLength) + ')')

  // Publish the form, and validate
  await search.clickElementCss(this.driver, '[name*=\'publish\']')
})

When('I complete the obituary form with an existing funeral', async function () {
  // Client details
  await data.enterTextInElementId(this.driver, 'form_firstname', 'Existing')
  await data.enterTextInElementId(this.driver, 'form_lastname', 'Client')
  await data.enterTextInElementId(this.driver, 'form_phone_number_r', '07234567890')
  await data.enterTextInElementId(this.driver, 'form_email_r', 'existingclient@test.com')

  // Deceased details
  await data.enterTextInElementId(this.driver, 'form_title', 'Existing')
  await data.enterTextInElementId(this.driver, 'form_last_name', 'Deceased')

  // Set date of birth
  await search.clickElementCss(this.driver, '#date-sel-dob-dd>option[value=\'1\']')
  await search.clickElementCss(this.driver, '#date-sel-dob-mm>option[value=\'1\']')
  await search.clickElementCss(this.driver, '#date-dob-sel>option[value=\'1950\']')

  // Set date of death
  await search.clickElementCss(this.driver, '#date-sel-dod-dd>option[value=\'1\']')
  await search.clickElementCss(this.driver, '#date-sel-dod-mm>option[value=\'1\']')
  await search.clickElementCss(this.driver, '#date-dod-sel>option[value=\'2018\']')

  // Funeral service details
  await search.clickElementCss(this.driver, '#autopopulateAddress_f_>option[value=\'Peterborough Crematorium\']')

  // Publish the form, and validate
  await search.clickElementCss(this.driver, '[name*=\'publish\']')
})

When('I complete the obituary form with missing contact details', async function () {
  // Client details, with no contact details
  await data.enterTextInElementId(this.driver, 'form_firstname', this.clientFirstName)
  await data.enterTextInElementId(this.driver, 'form_lastname', this.clientLastName)

  // Deceased details
  await data.enterTextInElementId(this.driver, 'form_title', this.deceasedFirstName)
  await data.enterTextInElementId(this.driver, 'form_last_name', this.deceasedLastName)

  // Set date of birth
  await search.clickElementCss(this.driver, '#date-sel-dob-dd>option[value=\'' + utility.getNumberBetweenTwoValues(1, 28) + '\']')
  await search.clickElementCss(this.driver, '#date-sel-dob-mm>option[value=\'' + utility.getNumberBetweenTwoValues(1, 12) + '\']')
  await search.clickElementCss(this.driver, 'date-dob-sel>option[value=\'' + utility.getNumberBetweenTwoValues(1920, 1990) + '\']')

  // Set date of death
  await search.clickElementCss(this.driver, '#date-sel-dod-dd>option[value=\'' + utility.getNumberBetweenTwoValues(1, 28) + '\']')
  await search.clickElementCss(this.driver, '#date-sel-dod-mm>option[value=\'' + utility.getNumberBetweenTwoValues(1, 12) + '\']')
  await search.clickElementCss(this.driver, '#date-dod-sel>option[value=\'2017\']')

  // Funeral service details
  const funeralServiceDropdown = await search.findElementsCss(this.driver, '#autopopulateAddress_f_>option')
  const funeralServiceDropdownLength = funeralServiceDropdown.length

  // Select the item in the dropdown
  await search.clickElementCss(this.driver, '#autopopulateAddress_f_>option:nth-of-type(' + utility.getNumberBetweenTwoValues(1, funeralServiceDropdownLength) + ')')

  // Publish the form, and validate
  await search.clickElementCss(this.driver, '[name*=\'publish\']')
})

When('I complete the obituary form with partial dob details', async function () {
  // Client details
  await data.enterTextInElementId(this.driver, 'form_firstname', this.clientFirstName)
  await data.enterTextInElementId(this.driver, 'form_lastname', this.clientLastName)
  await data.enterTextInElementId(this.driver, 'form_phone_number_r', '0747' + utility.getNumberBetweenTwoValues(1000000, 9999999))
  await data.enterTextInElementId(this.driver, 'form_email_r', this.clientEmail)

  // Deceased details
  await data.enterTextInElementId(this.driver, 'form_title', this.deceasedFirstName)
  await data.enterTextInElementId(this.driver, 'form_last_name', this.deceasedLastName)

  // Set partial date of birth
  await search.clickElementCss(this.driver, '#date-sel-dob-mm>option[value=\'' + utility.getNumberBetweenTwoValues(1, 12) + '\']')
  await search.clickElementCss(this.driver, '#date-dob-sel>option[value=\'' + utility.getNumberBetweenTwoValues(1920, 1990) + '\']')

  // Set date of death
  await search.clickElementCss(this.driver, '#date-sel-dod-dd>option[value=\'' + utility.getNumberBetweenTwoValues(1, 28) + '\']')
  await search.clickElementCss(this.driver, '#date-sel-dod-mm>option[value=\'' + utility.getNumberBetweenTwoValues(1, 12) + '\']')
  await data.enterTextInElementId(this.driver, 'date-dod-sel', '2017')

  // Funeral service details
  const funeralServiceDropdown = await search.findElementsCss(this.driver, '#autopopulateAddress_f_>option')
  const funeralServiceDropdownLength = funeralServiceDropdown.length

  // Select the item in the dropdown
  await search.clickElementCss(this.driver, '#autopopulateAddress_f_>option:nth-of-type(' + utility.getNumberBetweenTwoValues(1, funeralServiceDropdownLength) + ')')

  // Publish the form, and validate
  await search.clickElementCss(this.driver, '[name*=\'publish\']')
})

When('I complete the obituary form with partial dod details', async function () {
  // Client details
  await data.enterTextInElementId(this.driver, 'form_firstname', this.clientFirstName)
  await data.enterTextInElementId(this.driver, 'form_lastname', this.clientLastName)
  await data.enterTextInElementId(this.driver, 'form_phone_number_r', '0747' + utility.getNumberBetweenTwoValues(1000000, 9999999))
  await data.enterTextInElementId(this.driver, 'form_email_r', this.clientEmail)

  // Deceased details
  await data.enterTextInElementId(this.driver, 'form_title', this.deceasedFirstName)
  await data.enterTextInElementId(this.driver, 'form_last_name', this.deceasedLastName)

  // Set date of birth
  await search.clickElementCss(this.driver, '#date-sel-dob-dd>option[value=\'' + utility.getNumberBetweenTwoValues(1, 28) + '\']')
  await search.clickElementCss(this.driver, '#date-sel-dob-mm>option[value=\'' + utility.getNumberBetweenTwoValues(1, 12) + '\']')
  await search.clickElementCss(this.driver, '#date-dob-sel>option[value=\'' + utility.getNumberBetweenTwoValues(1920, 1990) + '\']')

  // Set partial date of death
  await search.clickElementCss(this.driver, '#date-sel-dod-dd>option[value=\'' + utility.getNumberBetweenTwoValues(1, 28) + '\']')
  await search.clickElementCss(this.driver, '#date-dod-sel>option[value=\'2018\']')

  // Funeral service details
  const funeralServiceDropdown = await search.findElementsCss(this.driver, '#autopopulateAddress_f_>option')
  const funeralServiceDropdownLength = funeralServiceDropdown.length

  // Select the item in the dropdown
  await search.clickElementCss(this.driver, '#autopopulateAddress_f_>option:nth-of-type(' + utility.getNumberBetweenTwoValues(1, funeralServiceDropdownLength) + ')')

  // Publish the form, and validate
  await search.clickElementCss(this.driver, '[name*=\'publish\']')
})

Then('The obituary edit is saved successfully', async function () {
  await search.simpleWaitCss(this.driver, '.alert--success')
  const updatedObit = await data.getTextFromElementCss(this.driver, '.obituaries.details>div:nth-of-type(2)>h3')
  assert(updatedObit !== this.currentObitDetail, 'The obituary was not updated successfully')
})

Then('I can successfully light a candle on the obituary', async function () {
  await search.clickElementId(this.driver, 'add_condolence_candle')
  await search.simpleWaitCss(this.driver, '.row-12.pull-left.candle-select')
  await search.clickElementCss(this.driver, '.sprite__candle.candle-sm--modern-yellow')
  await search.clickElementCss(this.driver, '#form_add_condolence_candle>form>div>div>div>button')
  await search.simpleWaitCss(this.driver, '.alert-message.success')

  // Validate the success message
  const successMsg = 'Your candle has been lit.'
  const successLabel = await data.getAttributeFromElementCss(this.driver, '.alert-message.success')
  const result = successLabel.includes(successMsg)
  assert.isTrue(result, 'The candle success message has not been found')
})

Then('I can successfully send a message on the obituary', async function () {
  await search.clickElementId(this.driver, 'add_condolence_message')
  await search.simpleWaitCss(this.driver, '#form_add_condolence_message>form>fieldset>div>textarea')
  await data.enterTextInElementCss(this.driver, '#form_add_condolence_message>form>fieldset>div>textarea', 'This is a test message')

  // Submit the message form
  await search.clickElementCss(this.driver, '#form_add_condolence_message>form>div>div>div>button')
  await search.simpleWaitCss(this.driver, '.alert-message')

  // Assert the success message is displayed
  const successText = 'Your condolence message has been posted.'
  const successLabel = data.getTextFromElementCss(this.driver, '.alert-message')
  const result = successLabel.includes(successText)
  assert.isTrue(result, 'The obituary message was not posted successfully')
})

Then('I see a partial date of birth validation error on the obituaries page', async function () {
  const dobValidationText = 'The deceased date of birth date must be complete or not set at all'
  await search.simpleWaitCss(this.driver, '.alert--error')
  const validationLabel = await data.getTextFromElementCss(this.driver, '.alert--error')
  const result = validationLabel.includes(dobValidationText)
  assert.isTrue(result, 'The partial date of birth validation text could not be found')
})

Then('I see a partial date of death validation error on the obituaries page', async function () {
  const dodValidationText = 'Please choose a deceased date of death'
  await search.simpleWaitCss(this.driver, '.alert--error')
  const validationLabel = await data.getTextFromElementCss(this.driver, '.alert--error')
  const result = validationLabel.includes(dodValidationText)
  assert.isTrue(result, 'The partial date of death validation text could not be found')
})

Then('The obituary is successfully created and I am on the obituaries screen', async function () {
  // Validate the success message on create obituary
  await search.simpleWaitCss(this.driver, '.alert--success')
  const successMessage = 'The obituary has been published on Funeral Zone and the client has been sent a link to take ownership of the obituary.'
  const successLabel = await data.getTextFromElementCss(this.driver, '.alert--success p')
  const result = successLabel.includes(successMessage)
  assert.isTrue(result, 'The create obituary success message has not been found')
})

Then('I see a duplicate user validation error on the form', async function () {
  // Check the duplicate user error is displayed
  await search.simpleWaitCss(this.driver, '.alert--error')
  const duplicateMessage = 'A funeral with these details already exists.'
  const duplicateLabel = await data.getTextFromElementCss(this.driver, '.alert--error p')
  const result = duplicateLabel.includes(duplicateMessage)
  assert.isTrue(result, 'The duplicate obituary message is incorrect, or has not been found')
})

Then('I see a contact details validation error on the form', async function () {
  // Check the duplicate user error is displayed
  await search.simpleWaitCss(this.driver, '.alert--error')
  const contactMessage = 'The email address is required if mobile phone number is omitted.\n' +
        'The mobile phone number is required if email address is omitted.'
  const contactLabel = await data.getTextFromElementCss(this.driver, '.alert--error')
  const result = contactLabel.includes(contactMessage)
  assert.isTrue(result, 'The missing contact details message is incorrect, or has not been found')
})
