const { Key } = require('selenium-webdriver')
const { Given, When, Then } = require('cucumber')
const assert = require('chai').assert
const search = require('../functions/searchMethods')
const data = require('../functions/dataMethods')
const utility = require('../functions/utilityMethods')

Given('I am on the admin > charities page', async function () {
  let menuLink, charityLink, expectedUrl, currentUrl
  await search.clickElementCss(this.driver, 'a[class=\'link-admin\']')
  menuLink = await search.simpleWaitCss(this.driver, '.dropdown:nth-of-type(5)')
  await menuLink.click()
  charityLink = await search.findElementCss(this.driver, 'a[href*=\'/admin/charities/list\']')
  await charityLink.click()
  await search.waitUntilUrlContains(this.driver, 'charities/list')
  expectedUrl = this.testUrl + '/admin/charities/list'
  currentUrl = await this.driver.getCurrentUrl()
  assert.strictEqual(expectedUrl, currentUrl, 'The charities page has not been found')
})

When('I select a charity from the list', async function () {
  let table, tableLength, rows
  table = await search.findElementsCss(this.driver, '.table>tbody>tr')
  tableLength = table.length

  for (rows = 1; rows < tableLength; rows++) {
    this.charityName = await data.getTextFromElementCss(this.driver, '.table>tbody>tr:nth-of-type(' + rows + ')>td:nth-of-type(1)')
    if (this.charityName.length > 0) {
      this.charityButton = await search.findElementCss(this.driver, '.table>tbody>tr:nth-of-type(' + rows + ')>td:nth-of-type(3)>a')
      // have to put a bit of login in here as firefox doesn't handle the table button click very well...
      if (process.env.BROWSER === 'firefox') {
        await this.driver.sleep(500)
        await this.charityButton.sendKeys(Key.ENTER)
      } else {
        await this.charityButton.click()
      }
      break
    }
  }
})

When('I add a contact to the charity', async function () {
  let charityFirstName, charityLastName
  await search.simpleWaitCss(this.driver, '.btn-primary')
  await search.clickElementCss(this.driver, '.btn-primary')
  await search.simpleWaitCss(this.driver, 'form_first_name')
  charityFirstName = 'First-' + utility.getRandomNumber(1000)
  charityLastName = 'Charity-' + utility.getRandomNumber(1000)
  this.charityEmail = charityLastName + '@test.com'
  await data.enterTextInElementId(this.driver, 'form_first_name', charityFirstName)
  await data.enterTextInElementId(this.driver, 'form_last_name', charityLastName)
  await data.enterTextInElementId(this.driver, 'form_email', this.charityEmail)
  await search.clickElementId(this.driver, 'form_Save')
})

Then('The contact is added successfully to the charity', async function () {
  let table, tableLength, rows, email
  await search.simpleWaitCss(this.driver, '.btn-primary')
  table = await search.findElementsCss(this.driver, '.table>tbody>tr')
  tableLength = table.length

  for (rows = 1; rows <= tableLength; rows++) {
    email = await data.getTextFromElementCss(this.driver, '.table>tbody>tr:nth-of-type(' + rows + ')>td:nth-of-type(2)')
    if (email === this.charityEmail) {
      assert.strictEqual(email, this.charityEmail, 'The charity contact has not been successfully added')
      break
    }
  }
})
