const { Given, When, Then } = require('cucumber')
const assert = require('chai').assert
const utility = require('../functions/utilityMethods')
const search = require('../functions/searchMethods')
const data = require('../functions/dataMethods')

Given('I am on the Funeral Directors page for the area of {string}', async function (area) {
  let currentUrl
  switch (area) {
    case area = 'Mitcham':
      await this.driver.get(this.testUrl + '/funeral-directors/london/mitcham')
      await search.waitUntilUrlContains(this.driver, 'mitcham')
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl === this.testUrl + '/funeral-directors/london/mitcham', 'The Mitcham Funeral Directors url is incorrect')
      break
    case area = 'South Molton':
      await this.driver.get(this.testUrl + '/funeral-directors/devon/south-molton')
      await search.waitUntilUrlContains(this.driver, 'south-molton')
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl === this.testUrl + '/funeral-directors/devon/south-molton', 'The South Molton Funeral Directors url is incorrect')
      break
  }
})

Given('I am on the {string} page', async function (page) {
  let currentUrl, result
  switch (page) {
    case page = 'funeral directors':
      await this.driver.get(this.testUrl + '/funeral-directors')
      await search.waitUntilUrlContains(this.driver, 'funeral-directors')
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl === this.testUrl + '/funeral-directors', 'The Funeral Directors url us incorrect, or the page did not load in time')
      break
    case page = 'help & resources':
      await this.driver.get(this.testUrl + '/help-resources')
      await search.waitUntilUrlContains(this.driver, 'help-resources')
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl === this.testUrl + '/help-resources', 'The Help & Resources url us incorrect, or the page did not load in time')
      break
    case page = 'obituaries':
      await this.driver.get(this.testUrl + '/obituaries')
      await search.waitUntilUrlContains(this.driver, 'obituaries')
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl === this.testUrl + '/obituaries', 'The Obituaries url us incorrect, or the page did not load in time')
      break
    case page = 'funeral wishes':
      // This url generates a random uuid, so check it contains the Funeral Wishes section
      await this.driver.get(this.testUrl + '/funeral-wishes')
      await search.waitUntilUrlContains(this.driver, 'funeral-wishes')
      currentUrl = await this.driver.getCurrentUrl()
      result = currentUrl.includes(this.testUrl + '/funeral-wishes#')
      assert.isTrue(result, 'The Funeral Wishes url us incorrect, or the page did not load in time')
      break
    case page = 'funeral plans':
      await this.driver.get(this.testUrl + '/funeral-plans')
      await search.waitUntilUrlContains(this.driver, 'funeral-plans')
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl === this.testUrl + '/funeral-plans', 'The Funeral Plans url us incorrect, or the page did not load in time')
      break
    case page = 'blog':
      await this.driver.get(this.testUrl + '/blog')
      await search.waitUntilUrlContains(this.driver, 'blog')
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl === this.testUrl + '/blog', 'The Blog url us incorrect, or the page did not load in time')
      break
    case page = 'about':
      await this.driver.get(this.testUrl + '/about')
      await search.waitUntilUrlContains(this.driver, 'about')
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl === this.testUrl + '/about', 'The About url us incorrect, or the page did not load in time')
      break
    case page = 'contact':
      await this.driver.get(this.testUrl + '/contact')
      await search.waitUntilUrlContains(this.driver, 'contact')
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl === this.testUrl + '/contact', 'The Contact url us incorrect, or the page did not load in time')
      break
    case page = 'features':
      await this.driver.get(this.testUrl + '/features')
      await search.waitUntilUrlContains(this.driver, 'features')
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl === this.testUrl + '/features', 'The Features url us incorrect, or the page did not load in time')
      break
    case page = 'directory':
      await this.driver.get(this.testUrl + '/directory')
      await search.waitUntilUrlContains(this.driver, 'directory')
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl === this.testUrl + '/directory', 'The Directory url us incorrect, or the page did not load in time')
      break
    case page = 'funeral directors a-z':
      await this.driver.get(this.testUrl + '/a-z/a')
      await search.waitUntilUrlContains(this.driver, 'a-z/a')
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl === this.testUrl + '/a-z/z', 'The Funeral Directors A-Z url us incorrect, or the page did not load in time')
      break
    case page = 'privacy policy':
      await this.driver.get(this.testUrl + '/privacy-policy')
      await search.waitUntilUrlContains(this.driver, 'privacy-policy')
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl === this.testUrl + '/privacy-policy', 'The Privacy Policy url us incorrect, or the page did not load in time')
      break
    case page = 'terms & conditions':
      await this.driver.get(this.testUrl + '/terms-and-conditions')
      await search.waitUntilUrlContains(this.driver, 'terms-and-conditions')
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl === this.testUrl + '/terms-and-conditions', 'The Terms & Conditions url us incorrect, or the page did not load in time')
      break
    case page = 'sitemap':
      await this.driver.get(this.testUrl + '/sitemap')
      await search.waitUntilUrlContains(this.driver, 'sitemap')
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl === this.testUrl + '/sitemap', 'The Sitemap url us incorrect, or the page did not load in time')
      break
    case page = 'london':
      // Navigate directly to the Loondon FD page
      await this.driver.get(this.testUrl + '/funeral-directors/london')
      await search.waitUntilUrlContains(this.driver, 'funeral-directors/london')
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl === this.testUrl + '/funeral-directors/london', 'The London Funeral Directors url us incorrect, or the page did not load in time')
      break
    case page = 'bereavement at work':
      await this.driver.get(this.testUrl + '/bereavement-at-work')
      await search.waitUntilUrlContains(this.driver, 'bereavement-at-work')
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl === this.testUrl + '/bereavement-at-work', 'The bereavement at work url is incorrect, or the page did not load in time')
  }
})
Given('I select the top funeral director page that is listed', async function () {
  let list, listAttribute, currentUrl
  // Select the top listing in the 'Most Popular' list
  list = await search.findElementCss(this.driver, '#searchResults>li:nth-of-type(1)>a')
  listAttribute = await data.getAttributeFromWebElement(this.driver, list, 'href')
  await list.click()
  await search.simpleWaitCss(this.driver, '.fdp-name')
  currentUrl = await this.driver.getCurrentUrl()
  assert.strictEqual(currentUrl, listAttribute, 'The Funeral Director profile page url does not match')
})

Given('I am on the obituaries page for Exeter', async function () {
  await this.driver.get(this.testUrl + '/obituaries/exeter')
  await search.waitUntilUrlContains(this.driver, 'obituaries/exeter')
  const currentUrl = await this.driver.getCurrentUrl()
  assert.strictEqual(currentUrl === this.testUrl + '/obituaries/exeter', 'The obituries page for Exeter was not found')
})

Given('I select the first obituary displayed', async function () {
  let obituary, obituaryAttribute, currentUrl
  // Selecting the obituary on the top left of the selection
  obituary = await search.findElementCss(this.driver, '.obituary-summary-grid>div:nth-of-type(1)>div>div>div:nth-of-type(2)>a')
  obituaryAttribute = await data.getAttributeFromWebElement(this.driver, obituary)
  await obituary.click()
  await search.simpleWaitCss(this.driver, 'section[class=\'obituary-images\']')
  currentUrl = await this.driver.getCurrentUrl()
  assert.strictEqual(currentUrl, obituaryAttribute, 'The obituary page was not successfully loaded')
})

Given('I click the {string} link', async function (page) {
  let expectedUrl, currentUrl
  // Employers
  switch (page) {
    case page = 'guidance for employers':
      await search.clickElementCss(this.driver, '.container.sans>div:nth-of-type(2)>p:nth-of-type(2)>a')
      await search.waitUntilUrlContains(this.driver, 'bereavement-at-work/employer')
      expectedUrl = this.testUrl + '/bereavement-at-work/employer'
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(expectedUrl, currentUrl, 'The bereavement at work - guidance for employers page was not found')
      break

    case page = 'back to work':
      await search.clickElementCss(this.driver, '.container.sans>div:nth-of-type(3)>p:nth-of-type(2)>a')
      await search.waitUntilUrlContains(this.driver, 'bereavement-at-work/back-to-work')
      expectedUrl = this.testUrl + '/bereavement-at-work/back-to-work'
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(expectedUrl, currentUrl, 'The bereavement at work - back to work support page was not found')
      break

    case page = 'workplace strategy':
      await search.clickElementCss(this.driver, '.container.sans>div:nth-of-type(4)>p:nth-of-type(2)>a')
      await search.waitUntilUrlContains(this.driver, 'bereavement-at-work/workplace-strategy')
      expectedUrl = this.testUrl + '/bereavement-at-work/workplace-strategy'
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(expectedUrl, currentUrl, 'The bereavement at work - workplace strategy page was not found')
      break

    case page = 'employer support':
      await search.clickElementCss(this.driver, '.container.sans>div:nth-of-type(5)>p:nth-of-type(2)>a')
      await search.waitUntilUrlContains(this.driver, 'bereavement-at-work/back-to-work')
      expectedUrl = this.testUrl + '/bereavement-at-work/back-to-work'
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(expectedUrl, currentUrl, 'The bereavement at work - employer support page was not found')
      break

      // Employees
    case page = 'coping with bereavement':
      await search.clickElementCss(this.driver, '.container.sans>div:nth-of-type(9)>p:nth-of-type(2)>a')
      await search.waitUntilUrlContains(this.driver, 'bereavement-at-work/coping-at-work')
      expectedUrl = this.testUrl + '/bereavement-at-work/coping-at-work'
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(expectedUrl, currentUrl, 'The bereavement at work - coping with bereavement page was not found')
      break

    case page = 'guide to bereavement leave':
      await search.clickElementCss(this.driver, '.container.sans>div:nth-of-type(10)>p:nth-of-type(2)>a')
      await search.waitUntilUrlContains(this.driver, 'bereavement-at-work/bereavement-leave')
      expectedUrl = this.testUrl + '/bereavement-at-work/bereavement-leave'
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(expectedUrl, currentUrl, 'The bereavement at work - guide to bereavement leave page was not found')
      break

    case page = 'parental bereavement leave':
      await search.clickElementCss(this.driver, '.container.sans>div:nth-of-type(11)>p:nth-of-type(2)>a')
      await search.waitUntilUrlContains(this.driver, 'bereavement-at-work/parental-bereavement-leave')
      expectedUrl = this.testUrl + '/bereavement-at-work/parental-bereavement-leave'
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(expectedUrl, currentUrl, 'The bereavement at work - parental bereavement leave page was not found')
      break
  }
})

When('I complete the new enquiry form {string}', async function (formType) {
  let planTypes = ['Yourself',
    'Spouse / Partner',
    'Parent',
    'Someone else']
  const seed = utility.getRandomNumber(10000)
  switch (formType) {
    case formType = 'without errors':
      // Complete the form with no validation errors
      await data.enterTextInElementId(this.driver, 'recipient', planTypes[utility.getRandomNumber(3)])
      await data.enterTextInElementId(this.driver, 'first_name', 'Name')
      await data.enterTextInElementId(this.driver, 'last_name', 'Surname-' + seed)
      await data.enterTextInElementId(this.driver, 'email_address', 'surname-' + seed + '@test.com')
      await data.enterTextInElementId(this.driver, 'phone_number', '07' + utility.getNumberBetweenTwoValues(100000000, 999999999))
      await search.clickElementId(this.driver, 'submit')
      break
    case formType = 'with errors':
      // Complete the form with some validation errors - TBC
      await data.enterTextInElementId(this.driver, 'recipient', planTypes[utility.getRandomNumber(3)])
      await data.enterTextInElementId(this.driver, 'first_name', 'Name')
      await data.enterTextInElementId(this.driver, 'last_name', 'Surname-' + seed)
      await data.enterTextInElementId(this.driver, 'email_address', 'surname-' + seed + '@test.com')
      await data.enterTextInElementId(this.driver, 'phone_number', 'abc')
      await search.clickElementId(this.driver, 'submit')
      break
  }
})

When('I check the page source {string}', async function (domElement) {
  let page
  // Grab the whole page and then set the innerHTML as a string we can use to find the tags we need
  switch (domElement) {
    case domElement = 'head':
      page = await search.findElementTagName(this.driver, 'head')
      this.head = await data.getAttributeFromWebElement(this.driver, 'innerHTML')
      assert(typeof this.head === 'string', 'The page head innerHTML was not stored corrrectly.  Should be a string, but is: ' + typeof this.head)
      break
    case domElement = 'body':
      page = await search.findElementTagName(this.driver, 'body')
      this.body = await page.getAttribute('innerHTML')
      assert(typeof this.body === 'string', 'The page body innerHTML was not stored corrrectly.  Should be a string, but is: ' + typeof this.body)
      break
  }
})

Then('I see a canonical link that links back to the {string} Funeral Directors page', async function (location) {
  let canonicalLink, result
  // Based on the location in the test, we check for the correct canonical links are available for the specified location
  switch (location) {
    case location = 'London':
      canonicalLink = '<link rel="canonical" href="' + this.testUrl + '/funeral-directors/london" itemprop="url">'
      result = this.head.includes(canonicalLink)
      assert.isTrue(result, 'The London/Mitcham canonical link could not be found')
      break
    case location = 'Main':
      canonicalLink = '<link rel="canonical" href="' + this.testUrl + '/funeral-directors" itemprop="url">'
      result = this.head.includes(canonicalLink)
      assert.isTrue(result, 'The South Molton canonical link could not be found')
      break
  }
})

Then('I see the Google Analytics tracking script', async function () {
  let gaScript, result
  // Complete GA script for page view
  gaScript = '    ga(\'require\', \'linkid\');\n    ga(\'create\', \'UA-33216372-1\', {\n      cookieDomain: \'auto\',\n      siteSpeedSampleRate: 10\n    });\n    ga(\'send\', \'pageview\');'
  result = this.body.includes(gaScript)
  assert.isTrue(result, 'The Google Analytics tracking code could not be found')
})

Then('I see the updated meta {string} for the Funeral Plans page', async function (metaType) {
  let metaTag, result
  switch (metaType) {
    case metaType = 'title':
      metaTag = 'Prepaid Funeral Plans - Funeral Zone'
      result = this.head.includes(metaTag)
      assert.isTrue(result, 'The updated funeral plans meta head was not found')
      break
    case metaType = 'description':
      metaTag = 'Freeze costs at today’s prices. Guaranteed acceptance. FPA regulated. Flexible payment options. Fixed price affordable plans. Green funeral options.'
      result = this.head.includes(metaTag)
      assert.isTrue(result, 'The updated funeral plans meta description was not found')
      break
    case metaType = 'open graph':
      // Check to see if a chunk of the open graph meta is visible
      metaTag = 'meta property="og:title" content="Find out more about prepaid funeral plans with Funeral Zone'
      result = this.head.includes(metaTag)
      assert.isTrue(result, 'The updated funeral plans open graph meta data was not found')
      break
    case metaType = 'twitter card':
      // Check to see if a chunk of the twitter meta is available
      metaTag = 'meta property="twitter:card" content="summary_large_image"'
      result = this.head.includes(metaTag)
      assert.isTrue(result, 'The updated funeral plans twitter card meta data was not found')
      break
  }
})

Then('The {string} tracking code is visible', async function (tc) {
  let code, result
  switch (tc) {
    case tc = 'dimension4':
      code = 'dimension4'
      result = this.body.includes(code)
      assert.isTrue(result, 'The dimension4 tracking code was not found in the page source')
      break
    case tc = 'dimension6':
      code = 'dimension6'
      result = this.body.includes(code)
      assert.isTrue(result, 'The dimension6 tracking code was not found in the page source')
      break
    case tc = 'dimension2':
      code = 'dimension2'
      result = this.body.includes(code)
      assert.isTrue(result, 'The dimension2 tracking code was not found in the page source')
      break
    case tc = 'dimension1':
      code = 'dimension1'
      result = this.body.includes(code)
      assert.isTrue(result, 'The dimension1 tracking code was not found in the page source')
      break
    case tc = 'dimension5':
      code = 'dimension5'
      result = this.body.includes(code)
      assert.isTrue(result, 'The dimension5 tracking code was not found in the page source')
      break
  }
})

Then('The dimension5 tracking code is not visible', async function () {
  const code = 'dimension5'
  const result = this.body.includes(code)
  assert.isFalse(result, 'The dimension5 tracking code was incorrectly found')
})

Then('I see the bereavement at work {string} meta data', async function (type) {
  let title, description, result
  switch (type) {
    case type = 'title':
      title = 'Bereavement In The Workplace'
      result = this.head.includes(title)
      assert.isTrue(result, 'The bereavement at work homepage title was not found in the page source head')
      break
    case type = 'description':
      description = 'Helpful bereavement support resources and information for businesses, employers and employees coping with grief and managing bereavement in the workplace'
      result = this.head.includes(description)
      assert.isTrue(result, 'The bereavement at work homepage description was not found in the page source head')
      break
  }
})

Then('I see the guidance for employers {string} meta data', async function (type) {
  let title, description, result
  switch (type) {
    case type = 'title':
      title = 'Bereavement In The Workplace - Guidance for Employers'
      result = this.head.includes(title)
      assert.isTrue(result, 'The bereavement at work - guidance for employers page title was not found in the page source head')
      break
    case type = 'description':
      description = 'Guidelines for businesses, employers and HR on bereavement support in the workplace'
      result = this.head.includes(description)
      assert.isTrue(result, 'The bereavement at work - guidance for employers page description was not found in the page head')
      break
  }
})

Then('I see the guide to bereavement leave {string} meta data', async function (type) {
  let title, description, result
  switch (type) {
    case type = 'title':
      title = 'A Guide To Bereavement Leave - Funeral Zone'
      result = this.head.includes(title)
      assert.isTrue(result, 'The bereavement at work - guide to bereavement leave page title was not found in the page head')
      break
    case type = 'description':
      description = 'Find out more about bereavement leave, what it is and how to approach your employer about getting time off after a bereavement'
      result = this.head.includes(description)
      assert.isTrue(result, 'The bereavement at work - guide to bereavement leave page description was not found in the page head')
      break
  }
})

Then('I see the parental bereavement leave {string} meta data', async function (type) {
  let title, description, result
  switch (type) {
    case type = 'title':
      title = 'Parental Bereavement Leave - Funeral Zone'
      result = this.head.includes(title)
      assert.isTrue(result, 'The bereavement at work - parental bereavement leave title was not found in the page head')
      break
    case type = 'description':
      description = 'Find out more about paid parental bereavement leave and when it is set to become law in the UK'
      result = this.head.includes(description)
      assert.isTrue(result, 'The bereavement at work - parental bereavement leave page description was not found in the page head')
      break
  }
})

Then('I see the workplace strategy {string} meta data', async function (type) {
  let title, description, result
  switch (type) {
    case type = 'title':
      title = 'Workplace Bereavement Strategy - Funeral Zone'
      result = this.head.includes(title)
      assert.isTrue(result, 'The bereavement at work - workplace strategy page title was not found in the page head')
      break
    case type = 'description':
      description = 'Guidelines for employers, managers and HR for creating bereavement policy and supporting bereaved employees in the workplace'
      result = this.head.includes(description)
      assert.isTrue(result, 'The bereavement at work - workplace strategy page description was not found in the page head')
      break
  }
})

Then('I see the coping with bereavement {string} meta data', async function (type) {
  let title, description, result
  switch (type) {
    case type = 'title':
      title = 'Coping With Bereavement At Work - Funeral Zone'
      result = this.head.includes(title)
      assert.isTrue(result, 'The bereavement at work - coping with bereavement page title was not found in the page head')
      break
    case type = 'description':
      description = 'Practical help and advice for employees about coping at work after a bereavement'
      result = this.head.includes(description)
      assert.isTrue(result, 'The bereavement at work - coping with bereavement page description was not found in the page head')
      break
  }
})
