const { Given } = require('cucumber')
const assert = require('chai').assert
const useragent = require('useragent')
const createFile = require('../functions/utilityMethods').createFile
const search = require('../functions/searchMethods')
const data = require('../functions/dataMethods')
const fs = require('fs')
require('dotenv').config()

Given('I am logged in as admin', async function () {
  let userAgent, agent, browserVersion, loginLink, emailField, passwordField, currentUrl
  await this.driver.get(this.testUrl)

  // Store the browser info for use in reporting
  userAgent = await this.driver.executeScript('return navigator.userAgent')
  agent = useragent.parse(userAgent)
  browserVersion = agent.toVersion()
  createFile('browserDetails.js')
  fs.writeFileSync(process.cwd() + '/browserDetails.js', browserVersion)

  loginLink = await search.waitForElementCss(this.driver, 'a[class=\'link-login\']')
  await loginLink.click()
  emailField = await search.waitForElementId(this.driver, 'form_username')
  passwordField = await search.findElementId(this.driver, 'form_password')
  await data.enterTextInElement(this.driver, emailField, process.env.USERNAMES)
  await data.enterTextInElement(this.driver, passwordField, process.env.PASSWORD)
  await search.clickElementId(this.driver, 'form_submit')
  await search.simpleWaitCss(this.driver, 'div[class*=\'alert--success\']')

  currentUrl = await this.driver.getCurrentUrl()
  assert.strictEqual(currentUrl, this.testUrl + '/account', 'The admin logged in url is incorrect.\nFound: ' + currentUrl + ', but expected: ' + this.testUrl + '/account')
})

Given('I have logged in as user {string}', async function (user) {
  /*
      * As we now have anonymized the DB, we have to use the user id to impersonate a user
      * Users are as follows:
      * Central Coop: 193919
      * Lymn: 110586
      * Murray: 1783
      * J. Vernon: 82012
      * Dignity: 133525
      * Joseph Roberts: 216997
  */

  let expectedHeader, actualHeader
  switch (user) {
    case user = '193919':
      // Log in as coop user
      await this.driver.get(this.testUrl + '/forceLogin/193919')
      await search.simpleWaitCss(this.driver, '.sm-flex-column-reverse>div>h4')
      expectedHeader = 'Your dashboard for Central England Co-operative'
      actualHeader = await data.getTextFromElementCss(this.driver, '.sm-flex-column-reverse>div>h4')
      assert.strictEqual(expectedHeader, actualHeader, 'The user dashboard has not loaded correctly')
      break
    case user = '110586':
      // Log in as Lymn user
      await this.driver.get(this.testUrl + '/forceLogin/110586')
      await search.simpleWaitCss(this.driver, '.sm-flex-column-reverse>div>h4')
      expectedHeader = 'Your dashboard for A W Lymn'
      actualHeader = await data.getTextFromElementCss(this.driver, '.sm-flex-column-reverse>div>h4')
      assert.strictEqual(expectedHeader, actualHeader, 'The user dashboard has not loaded correctly')
      break
    case user = '1783':
      // Log in as Murray user
      await this.driver.get(this.testUrl + '/forceLogin/1783')
      await search.simpleWaitCss(this.driver, '.sm-flex-column-reverse>div>h4')
      expectedHeader = 'Your dashboard for Murray’s Independent Funeral Directors'
      actualHeader = await data.getTextFromElementCss(this.driver, '.sm-flex-column-reverse>div>h4')
      assert.strictEqual(expectedHeader, actualHeader, 'The user dashboard has not loaded correctly')
      break
    case user = '82012':
      // Log in as J Vernon user
      await this.driver.get(this.testUrl + '/forceLogin/82012')
      await search.simpleWaitCss(this.driver, '.sm-flex-column-reverse>div>h4')
      expectedHeader = 'Your dashboard for J. Vernon Kendrick Funeral Directors'
      actualHeader = await data.getTextFromElementCss(this.driver, '.sm-flex-column-reverse>div>h4')
      assert.strictEqual(expectedHeader, actualHeader, 'The user dashboard has not loaded correctly')
      break
    case user = '133525':
      // Log in as Dignity user
      await this.driver.get(this.testUrl + '/forceLogin/133525')
      await search.simpleWaitCss(this.driver, '.sm-flex-column-reverse>div>h4')
      expectedHeader = 'Your dashboard for Dignity UK PLC'
      actualHeader = await data.getTextFromElementCss(this.driver, '.sm-flex-column-reverse>div>h4')
      assert.strictEqual(expectedHeader, actualHeader, 'The user dashboard has not loaded correctly')
      break
    case user = '216997':
      // Log in as Joseph Roberts user
      await this.driver.get(this.testUrl + '/forceLogin/216997')
      await search.simpleWaitCss(this.driver, '.sm-flex-column-reverse>div>h4')
      expectedHeader = 'Your dashboard for Joseph C Roberts'
      actualHeader = await data.getTextFromElementCss(this.driver, '.sm-flex-column-reverse>div>h4')
      assert.strictEqual(expectedHeader, actualHeader, 'The user dashboard has not loaded correctly')
      break
  }
})
