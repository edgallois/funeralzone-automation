const { When, Then } = require('cucumber')
const assert = require('chai').assert
const search = require('../functions/searchMethods')
const data = require('../functions/dataMethods')

When('I am on the reviews page', async function () {
  await search.clickElementCss(this.driver, 'a[class=\'btn\'][href*=\'/home/reviews\']')
  await search.waitUntilUrlContains(this.driver, '/home/reviews')
  const currentUrl = await this.driver.getCurrentUrl()
  assert.strictEqual(currentUrl, this.testUrl + '/account/home/reviews', 'The reviews page was not successfully loaded')
})

Then('I can reply successfully to a listed review', async function () {
  let card, i, replyButton, editButton
  await search.simpleWaitCss(this.driver, '.btn--filter-review.on')
  card = await search.findElementsCss(this.driver, '.mini-card')
  i = 0
  while (i <= card.length) {
    replyButton = await search.checkElementExists(this.driver, card[i])
    if (replyButton === true) {
      this.row = i
      await search.clickElementCss(this.driver, '.mini-card:nth-of-type(' + this.row + ')>div:nth-of-type(2)' +
                '>div[id*=\'reply\'][class=\'reply-container\']>a')
      await data.enterTextInElementCss(this.driver, '.mini-card:nth-of-type(' + this.row + ')' +
                '>div:nth-of-type(2)>div:nth-of-type(3)>div>form>textarea', 'This is a test reply')
      await search.clickElementCss(this.driver, '.mini-card:nth-of-type(' + this.row + ')' +
                '>div:nth-of-type(2)>div:nth-of-type(3)>div>form>div>span:nth-of-type(1)>button')
      await search.simpleWaitCss(this.driver, '.mini-card:nth-of-type(' + this.row + ')>div:nth-of-type(2)>div:nth-of-type(4)>p[class=\'message\']')

      // assert that the edit button is now shown
      editButton = await search.checkItemExistsCss(this.driver, '.mini-card:nth-of-type(' + this.row + ')>div:nth-of-type(2)' +
      '>div[id*=\'reply\'][class=\'reply-container\']>a')

      assert.isTrue(editButton, 'The edit button was not located after replying to a review')
      break
    }
    i++
  }
})
