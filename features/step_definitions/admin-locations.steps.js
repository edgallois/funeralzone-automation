const { Given, When, Then } = require('cucumber')
const assert = require('chai').assert
const search = require('../functions/searchMethods')
const data = require('../functions/dataMethods')

Given('I am on the admin > directory > {string} page', async function (linkType) {
  // Navigate to the admin locations page
  await search.clickElementCss(this.driver, 'a[class=\'link-admin\']')
  await search.clickElementCss(this.driver, '.dropdown:nth-of-type(4)')

  // Select the area in the 'Directory' section
  switch (linkType) {
    case linkType = 'Regions':
      await search.clickElementCss(this.driver, 'a[href*=\'/regions\']')
      break
    case linkType = 'Businesses':
      await search.clickElementCss(this.driver, 'a[href*=\'/business\']')
      break
    case linkType = 'Categories':
      await search.clickElementCss(this.driver, 'a[href*=\'/categories\']')
      break
    case linkType = 'Locations':
      await search.clickElementCss(this.driver, 'a[href*=\'/locations\']')
      break
  }
})

When('I click the {string} button on the locations page', async function (buttonType) {
  await search.simpleWaitId(this.driver, 'form_name')
  switch (buttonType) {
    case buttonType = 'Manage Area Types':
      await search.clickElementCss(this.driver, '.btn-info')
      break
    case buttonType = 'Add New Location':
      const newLocation = await search.findElementsCss(this.driver, 'a[href*=\'locations/add\']')
      await newLocation[0].click()
      break
  }
})

When('I click the edit button on the area types page for a {string}', async function (locationType) {
  switch (locationType) {
    case locationType = 'county':
      await search.clickElementCss(this.driver, '.table>tbody>tr:nth-of-type(1)>td:nth-of-type(6)>a')
      break
    case locationType = 'town':
      await search.clickElementCss(this.driver, '.table>tbody>tr:nth-of-type(2)>td:nth-of-type(6)>a')
      break
  }
})

When('I complete the add location flow for a new {string} location', async function (locationType) {
  if (locationType === 'county') {
    await data.enterTextInElementId(this.driver, 'form_name', this.testCounty)
    await data.enterTextInElementId(this.driver, 'form_slug', this.testCountySlug)
    await search.clickElementId(this.driver, 'form_submit')
    await search.simpleWaitId(this.driver, 'form_name')
  } else if (locationType === 'town') {
    await data.enterTextInElementId(this.driver, 'form_name', this.testTown)
    await data.enterTextInElementId(this.driver, 'form_slug', this.testTownSlug)
    await data.enterTextInElementId(this.driver, 'form_area_type_id', 'Town')
    await data.enterTextInElementId(this.driver, 'form_parent_id', 'Devon')
    await search.clickElementId(this.driver, 'form_submit')
    await search.simpleWaitId(this.driver, 'form_name')
  }
})

When('I click the edit button next to a {string}', async function (locationType) {
  if (locationType === 'county') {
    await data.enterTextInElementId(this.driver, 'form_name', 'Test County')
    await search.clickElementCss(this.driver, 'button[type=\'submit\']')
    await search.waitUntilUrlContains(this.driver, 'name=Test+County')
    this.county = await data.getTextFromElementCss(this.driver, '.table>tbody>tr:nth-of-type(1)>td:nth-of-type(1)')
    await search.clickElementCss(this.driver, '.table>tbody>tr:nth-of-type(1)>td:nth-of-type(7)>a[href*=\'edit\']')
  } else if (locationType === 'town') {
    await data.enterTextInElementId(this.driver, 'form_name', 'Test Town')
    await search.clickElementCss(this.driver, 'button[type=\'submit\']')
    await search.waitUntilUrlContains(this.driver, 'name=Test+Town')
    this.town = await data.getTextFromElementCss(this.driver, '.table>tbody>tr:nth-of-type(1)>td:nth-of-type(1)')
    await search.clickElementCss(this.driver, '.table>tbody>tr:nth-of-type(1)>td:nth-of-type(7)>a[href*=\'edit\']')
  }
})

Then('I can edit the {string} name successfully', async function (locationType) {
  let name
  await search.waitUntilUrlContains(this.driver, '/locations/edit')
  if (locationType === 'county') {
    await data.enterTextInElementId(this.driver, 'form_name', 'Edit County')
    await search.clickElementId(this.driver, 'form_submit')
    await search.waitUntilUrlIs(this.driver, this.testUrl + '/admin/directory/locations/list')

    // Search for the edited location
    await data.enterTextInElementId(this.driver, 'form_name', 'Edit County')
    await search.clickElementCss(this.driver, 'button[type=\'submit\']')
    await search.waitUntilUrlContains(this.driver, 'name=Edit+County')
    name = await data.getTextFromElementCss(this.driver, '.table>tbody>tr:nth-of-type(1)>td:nth-of-type(1)')
    assert.notEqual(name, this.county, 'The edited county was not saved')
  } else if (locationType === 'town') {
    await data.enterTextInElementId(this.driver, 'form_name', 'Edit Town')
    await search.clickElementId(this.driver, 'form_submit')
    await search.waitUntilUrlIs(this.driver, this.testUrl + '/admin/directory/locations/list')

    // Search for the edited location
    await data.enterTextInElementId(this.driver, 'form_name', 'Edit Town')
    await search.clickElementCss(this.driver, 'button[type=\'submit\']')
    await search.waitUntilUrlContains(this.driver, 'name=Edit+Town')
    name = await data.getTextFromElementCss(this.driver, '.table>tbody>tr:nth-of-type(1)>td:nth-of-type(1)')
    assert.notEqual(name, this.town, 'The edited town was not saved')
  }
})

Then('The new {string} location is successfully created', async function (locationType) {
  let name
  // Search for the newly created location
  if (locationType === 'county') {
    // Search for the created location
    await search.waitUntilUrlContains(this.driver, '/admin/directory/locations/list')
    await data.enterTextInElementId(this.driver, 'form_name', this.testCounty)
    await search.clickElementCss(this.driver, 'button[type=\'submit\']')
    name = await data.getTextFromElementCss(this.driver, '.table>tbody>tr:nth-of-type(1)>td:nth-of-type(1)')
    assert.strictEqual(name, this.testCounty, 'The created county was not found')
  } else if (locationType === 'town') {
    // Search for the created location
    await search.waitUntilUrlContains(this.driver, '/admin/directory/locations/list')
    await data.enterTextInElementId(this.driver, 'form_name', this.testTown)
    await search.clickElementCss(this.driver, 'button[type=\'submit\']')
    name = await data.getTextFromElementCss(this.driver, '.table>tbody>tr:nth-of-type(1)>td:nth-of-type(1)')
    assert.strictEqual(name, this.testTown, 'The created county was not found')
  }
})

Then('I can successfully make changes to the {string} area type {string}', async function (location, type) {
  let name, currentName, slug, currentSlug, townName, townSlug
  // Edit based on the location type and the field to edit
  switch (location) {
    case location = 'county':
      if (type === 'name') {
        name = await search.findElementId(this.driver, 'form_name')
        this.originalCountyName = await name.getText()
        await data.enterTextInElement(this.driver, name, 'County-name')
        await search.clickElementId(this.driver, 'form_submit')

        // check that the details saved
        await search.waitUntilUrlIs(this.driver, this.testUrl + '/admin/directory/areatypes/list')
        currentName = await data.getTextFromElementCss(this.driver, '.table>tbody>tr:nth-of-type(1)>td:nth-of-type(1)')
        assert.strictEqual(currentName, 'County-name', 'The updated County name was not saved')
      } else if (type === 'slug') {
        slug = await search.findElementId(this.driver, 'form_slug')
        this.originalCountySlug = await slug.getText()
        await data.enterTextInElement(this.driver, slug, 'county-slug')
        await search.clickElementId(this.driver, 'form_submit')

        // check that the details saved
        await search.waitUntilUrlIs(this.driver, this.testUrl + '/admin/directory/areatypes/list')
        currentSlug = await data.getTextFromElementCss(this.driver, '.table>tbody>tr:nth-of-type(1)>td:nth-of-type(2)')
        assert.strictEqual(currentSlug, 'county-slug', 'The updated County slug was not saved')
      }
      break
    case location = 'town':
      if (type === 'name') {
        name = await search.findElementId(this.driver, 'form_name')
        this.originalTownName = await name.getText()
        await data.enterTextInElement(this.driver, name, 'Town-name')
        await search.clickElementId(this.driver, 'form_submit')

        // check that the details saved
        await search.waitUntilUrlIs(this.driver, this.testUrl + '/admin/directory/areatypes/list')
        townName = await data.getTextFromElementCss(this.driver, '.table>tbody>tr:nth-of-type(2)>td:nth-of-type(1)')
        assert.strictEqual(townName, 'Town-name', 'The updated Town name was not saved')
      } else if (type === 'slug') {
        slug = await search.findElementId(this.driver, 'form_slug')
        this.originalTownSlug = await slug.getText()
        await data.enterTextInElement(this.driver, slug, 'town-slug')
        await search.clickElementId(this.driver, 'form_submit')

        // check that the details saved
        await search.waitUntilUrlIs(this.driver, this.testUrl + '/admin/directory/areatypes/list')
        townSlug = await data.getTextFromElementCss(this.driver, '.table>tbody>tr:nth-of-type(2)>td:nth-of-type(2)')
        assert.strictEqual(townSlug, 'town-slug', 'The updated Town slug name was not saved')
      }
      break
  }
})

Then('The area types page loads successfully', async function () {
  await search.waitUntilUrlContains(this.driver, 'areatypes')
  const currentUrl = await this.driver.getCurrentUrl()
  const expectedUrl = this.testUrl + '/admin/directory/areatypes/list'
  assert.strictEqual(currentUrl, expectedUrl, 'We are not on the area types page, or the page load failed')
})

Then('The add new location page loads successfully', async function () {
  await search.waitUntilUrlContains(this.driver, 'locations/add')
  const currentUrl = await this.driver.getCurrentUrl()
  const expectedUrl = this.testUrl + '/admin/directory/locations/add'
  assert.strictEqual(currentUrl, expectedUrl, 'We are not on the add location page, or the page load failed')
})
