const { Given, When, Then } = require('cucumber')
const { Key } = require('selenium-webdriver')
const assert = require('chai').assert
const utility = require('../functions/utilityMethods')
const search = require('../functions/searchMethods')
const data = require('../functions/dataMethods')

Given('I have impersonated user {string}', async function (userName) {
  let table, i, row, expectedName, result, expectedUrl, currentUrl, updatedUrl
  // A duplicate user would have been created manually, so search for this user type and login
  await this.driver.get(this.testUrl + '/admin/users?group=1')
  await data.enterTextInElementId(this.driver, 'u_email', userName)
  await data.enterTextInElementId(this.driver, 'u_email', Key.ENTER)
  await search.waitUntilUrlContains(this.driver, 'email=bereaveduser')

  table = await search.findElementsCss(this.driver, '.table>tbody>tr')
  for (i = 0; i <= table.length; i++) {
    row = i + 1
    expectedName = await data.getTextFromElementCss(this.driver, '.table>tbody>tr:nth-of-type(' + row + ')>td:nth-of-type(6)')
    result = expectedName.includes(userName)
    if (result === true) {
      this.id = await data.getTextFromElementCss(this.driver, '.table>tbody>tr:nth-of-type(' + row + ')>td:nth-of-type(1)')
      await this.driver.get(this.testUrl + '/forceLogin/' + this.id)
      break
    }
  }

  expectedUrl = this.testUrl + '/account/dashboard/bereaved'
  currentUrl = await this.driver.getCurrentUrl()
  // If first time visit, click through the confirm prompts
  if (currentUrl === this.testUrl + '/agree-terms') {
    await search.clickElementId(this.driver, 'form_submit')
    await search.simpleWaitCss(this.driver, '#email_contacts_form>.pull-left>a')
    await search.clickElementCss(this.driver, '#email_contacts_form>.pull-left>a')
    await search.simpleWaitCss(this.driver, '.btn.primary')
    await search.clickElementCss(this.driver, '.btn.primary')
    await search.simpleWaitCss(this.driver, '.circle.frame')
    updatedUrl = await this.driver.getCurrentUrl()
    assert.strictEqual(updatedUrl, expectedUrl, 'The bereaved client dashboard was not successfully loaded')
  } else {
    // Expect to be on the bereaved dashboard
    await search.simpleWaitCss(this.driver, '.circle.frame')
    updatedUrl = await this.driver.getCurrentUrl()
    assert.strictEqual(updatedUrl, expectedUrl, 'The bereaved client dashboard was not successfully loaded')
  }
})

Given('I have clicked the leave review button on the bereaved dashboard', async function () {
  let expectedUrl, currentUrl, result
  await search.simpleWaitCss(this.driver, 'a[href*=\'account/reviews\']')
  await search.clickElementCss(this.driver, 'a[href*=\'account/reviews\']')
  expectedUrl = this.testUrl + '/account/reviews/funeral/'
  currentUrl = await this.driver.getCurrentUrl()
  result = currentUrl.includes(expectedUrl)
  assert.isTrue(result, 'The reviews url is incorrect, or we are on the wrong page')
})

When('I edit the loved ones details', async function () {
  await search.clickElementCss(this.driver, '.icon-pencil')
  await search.simpleWaitId(this.driver, 'form_title')

  this.originalFirstName = await data.getAttributeFromElementId(this.driver, 'form_title', 'value')
  this.originalLastName = await data.getAttributeFromElementId(this.driver, 'form_last_name', 'value')
  this.editedFirstName = this.originalFirstName + '-edited'
  this.editedLastName = this.originalLastName + '-edited'

  await data.enterTextInElementId(this.driver, 'form_title', this.editedFirstName)
  await data.enterTextInElementId(this.driver, 'form_title', this.editedLastName)
  await search.clickElementCss(this.driver, '.pull-left.btn-wrapper')
  await search.simpleWaitCss(this.driver, '.alert--success')
  const successText = 'The obituary has been updated'
  const successLabel = await data.getTextFromElementCss(this.driver, '.alert--success')
  const result = successLabel.includes(successText)
  assert.isTrue(result, 'The bereaved obituary edit did not save successfully')
})

When('I complete the leave review process', async function () {
  await search.clickElementId(this.driver, 'rating3')
  await data.enterTextInElementCss(this.driver, '.pure-input-1', 'This is a test review')
  await search.clickElementCss(this.driver, 'input[value*=\'Submit\']')

  // This isn't currently working on local, so adding as placeholder
  await search.simpleWaitCss(this.driver, '.alert-success')
  const successText = 'Your review has been sent'
  const successLabel = await data.getTextFromElementCss(this.driver, '.alert-success')
  const result = successLabel.includes(successText)
  assert.isTrue(result, 'The review was not posted successfully')
})

When('I create an obituary with the bereaved email address {string}', async function (email) {
  // Client details
  await data.enterTextInElementId(this.driver, 'form_firstname', 'Bereaved')
  await data.enterTextInElementId(this.driver, 'form_lastname', 'User')
  await data.enterTextInElementId(this.driver, 'form_phone_number_r', '0747' + utility.getNumberBetweenTwoValues(1000000, 9999999))
  await data.enterTextInElementId(this.driver, 'form_email_r', email)

  // Deceased details
  await data.enterTextInElementId(this.driver, 'form_title', 'Bereaved')
  await data.enterTextInElementId(this.driver, 'form_last_name', 'Deceased')

  // Set date of birth
  await search.clickElementCss(this.driver, '#date-sel-dob-dd>option[value=\'1\']')
  await search.clickElementCss(this.driver, '#date-sel-dob-mm>option[value=\'1\']')
  await search.clickElementCss(this.driver, '#date-dob-sel>option[value=\'1950\']')

  // Set date of death
  await search.clickElementCss(this.driver, '#date-sel-dod-dd>option[value=\'1\']')
  await search.clickElementCss(this.driver, '#date-sel-dod-mm>option[value=\'4\']')
  await search.clickElementCss(this.driver, '#date-dod-sel>option[value=\'2019\']')

  // Funeral service details
  const funeralServiceDropdown = await search.findElementsCss(this.driver, '#autopopulateAddress_f_>option')
  const funeralServiceDropdownLength = funeralServiceDropdown.length
  await search.clickElementCss(this.driver, '#autopopulateAddress_f_>option:nth-of-type(' + utility.getNumberBetweenTwoValues(1, funeralServiceDropdownLength) + ')')
  await search.clickElementCss(this.driver, '[name*=\'publish\']')
  await search.simpleWaitCss(this.driver, '.container-wide')
  await search.clickElementCss(this.driver, '.nav-secondary>a[href=\'/logout\']')
  await search.waitUntilUrlContains(this.driver, 'logout')
  const expectedUrl = this.testUrl + '/logout'
  const currentUrl = await this.driver.getCurrentUrl()
  assert.strictEqual(expectedUrl, currentUrl, 'The logout page was not found')
})

Then('The bereaved edit is successfully saved', async function () {
  let editedName, editedTitle, result
  await search.clickElementCss(this.driver, '.rhs>section>div>div>a[href=\'/account\']')
  await search.simpleWaitCss(this.driver, '.circle.frame')
  editedName = this.editedFirstName + ' ' + this.editedLastName
  editedTitle = await data.getTextFromElementCss(this.driver, '.one-whole>h3')
  result = editedTitle.includes(editedName)
  assert.isTrue(result, 'The edited bereaved name has not been successfully saved')

  // reset values to default
  await this.driver.get(this.testUrl + '/account/obituaries/45325/obituary-basic')
  await data.enterTextInElementId(this.driver, 'form_title', this.originalFirstName)
  await data.enterTextInElementId(this.driver, 'form_last_name', this.originalLastName)
  await search.clickElementCss(this.driver, '.pull-left.btn-wrapper')
})

Then('I can log into the {string} account', async function (email) {
  let table, i, row, activate, currentEmail, result, expectedDashboard, currentUrl
  await search.clickElementCss(this.driver, '.nav-secondary>a[href=\'/admin\']')
  await this.driver.get(this.testUrl + '/admin/users?group=1')

  // find the email in the table and click the activate button
  table = await search.findElementsCss(this.driver, '.table>tbody>tr')
  for (i = 0; i <= table.length; i++) {
    row = i + 1
    currentEmail = await data.getTextFromElementCss(this.driver, '.table>tbody>tr:nth-of-type(' + row + ')>td:nth-of-type(6)')
    result = currentEmail.includes(email)
    if (result === true) {
      this.id = await data.getTextFromElementCss(this.driver, '.table>tbody>tr:nth-of-type(' + row + ')>td:nth-of-type(1)')
      activate = await search.checkItemExistsCss(this.driver, '.table>tbody>tr:nth-of-type(' + row + ')>td:nth-of-type(8)>a[class*=\'btn-danger\']')
      if (activate === true) {
        await search.clickElementCss(this.driver, '.table>tbody>tr:nth-of-type(' + row + ')>td:nth-of-type(8)>a[class*=\'btn-danger\']')
        await search.simpleWaitCss(this.driver, '.container-wide')
        await this.driver.get(this.testUrl + '/forceLogin/' + this.id)
        expectedDashboard = this.testUrl + '/account/dashboard/bereaved'
        currentUrl = await this.driver.getCurrentUrl()
        assert.strictEqual(expectedDashboard, currentUrl, 'The bereaved dashboard was not found')
      } else if (activate === false) {
        await this.driver.get(this.testUrl + '/forceLogin/' + this.id)
        expectedDashboard = this.testUrl + '/account/dashboard/bereaved'
        currentUrl = await this.driver.getCurrentUrl()
        assert.strictEqual(expectedDashboard, currentUrl, 'The bereaved dashboard was not found')
      }
      break
    }
  }
})
