const { Given, When, Then } = require('cucumber')
const assert = require('chai').assert
const search = require('../functions/searchMethods')
const data = require('../functions/dataMethods')

Given('I am on the Funeral Zone homepage', async function () {
  await this.driver.get(this.testUrl)
  await search.simpleWaitCss(this.driver, 'h1 b')
  const url = await this.driver.getCurrentUrl()
  assert.strictEqual(url, this.testUrl + '/', 'The homepage url is incorrect')
})

Given('I am on a funeral directors listing page', async function () {
  await this.driver.get(this.testUrl + '/funeral-directors/county-antrim')
  await search.simpleWaitCss(this.driver, '.flush--bottom.primer.kirk')
  const expectedUrl = this.testUrl + '/funeral-directors/county-antrim'
  const currentUrl = await this.driver.getCurrentUrl()
  assert.strictEqual(currentUrl, expectedUrl, 'We are not on the correct funeral directors page, or the page did not load')
})

When('I click a review on the page', async function () {
  this.linkName = await data.getTextFromElementCss(this.driver, '.hold-reviews>div:nth-of-type(1)>div>div>a')
  await search.clickElementCss(this.driver, '.hold-reviews>div:nth-of-type(1)>div>div>a')
  await search.simpleWaitCss(this.driver, '.fdb__summary>h1')
})

When('I click the {string} link in the navigation bar', async function (linkType) {
  switch (linkType) {
    case linkType = 'Compare Funeral Directors':
      await search.clickElementCss(this.driver, '.p-nav__wrapper a[href*=\'funeral-directors\']')
      break
    case linkType = 'Help & Resources':
      await search.clickElementCss(this.driver, '.p-nav__wrapper a[href*=\'help-resources\']')
      break
    case linkType = 'Obituaries':
      await search.clickElementCss(this.driver, '.p-nav__wrapper a[href*=\'obituaries\']')
      break
    case linkType = 'Funeral Wishes':
      await search.clickElementCss(this.driver, '.p-nav__wrapper a[href*=\'funeral-wishes\']')
      break
    case linkType = 'Funeral Plans':
      await search.clickElementCss(this.driver, '.p-nav__wrapper a[href*=\'funeral-plans\']')
      break
    case linkType = 'Funeral Directors - Join Now':
      await search.clickElementCss(this.driver, '.p-nav__wrapper a[href*=\'register/funeral-director\']')
      break
    case linkType = 'Blog':
      await search.clickElementCss(this.driver, '.nav-secondary>a[href*=\'blog\']')
      break
  }
})

When('I click the {string} link in the footer', async function (linkType) {
  switch (linkType) {
    case linkType = 'About':
      await search.clickElementCss(this.driver, '.smaller[href*=\'about\']')
      break
    case linkType = 'Contact':
      await search.clickElementCss(this.driver, '.smaller[href*=\'contact\']')
      break
    case linkType = 'Blog':
      await search.clickElementCss(this.driver, '.smaller[href*=\'blog\']')
      break
    case linkType = 'Features':
      await search.clickElementCss(this.driver, '.smaller[href*=\'features\']')
      break
    case linkType = 'Directory':
      await search.clickElementCss(this.driver, '.smaller[href*=\'directory\']')
      break
    case linkType = 'Funeral Directors A-Z':
      await search.clickElementCss(this.driver, '.smaller[href*=\'a-z/a\']')
      break
    case linkType = 'Privacy':
      await search.clickElementCss(this.driver, '.smaller[href*=\'privacy-policy\']')
      break
    case linkType = 'Terms of Use':
      await search.clickElementCss(this.driver, '.smaller[href*=\'terms-and-conditions\']')
      break
    case linkType = 'Sitemap':
      await search.clickElementCss(this.driver, '.smaller[href*=\'sitemap\']')
      break
  }
})

Then('I am taken to the business profile page for the review', async function () {
  const headText = await data.getTextFromElementCss(this.driver, '.fdb__summary>h1')
  assert.strictEqual(headText, this.linkName, 'We have not been taken to the correct business profile page')
})

Then('The {string} page should load successfully', async function (pageType) {
  let currentUrl
  switch (pageType) {
    case pageType = 'Compare Funeral Directors':
      await search.waitUntilUrlContains(this.driver, 'funeral-directors')
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl, this.testUrl + '/funeral-directors', 'The funeral directors page url is incorrect')
      break
    case pageType = 'Help & Resources':
      await search.waitUntilUrlContains(this.driver, 'help-resources')
      currentUrl = await this.driver.getCurrentUrl()
      assert(currentUrl === this.testUrl + '/help-resources', 'The help & resources page url is incorrect')
      break
    case pageType = 'Obituaries':
      await this.driver.wait(until.urlContains('obituaries'), 10000)
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl, this.testUrl + '/obituaries', 'The obituaries page url is incorrect')
      break
    case pageType = 'Funeral Wishes':
      await search.waitUntilUrlContains(this.driver, 'funeral-wishes')
      currentUrl = await this.driver.getCurrentUrl()
      const url = this.testUrl + '/funeral-wishes'
      const result = currentUrl.includes(url)
      assert.isTrue(result, 'The funeral wishes page url is incorrect')
      break
    case pageType = 'Funeral Plans':
      await search.waitUntilUrlContains(this.driver, 'funeral-plans')
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl, this.testUrl + '/funeral-plans', 'The funeral plans page url is incorrect')
      break
    case pageType = 'Funeral Directors - Join Now':
      await search.waitUntilUrlContains(this.driver, 'register/funeral-director')
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl, this.testUrl + '/register/funeral-director', 'The funeral directors - join now page url is incorrect')
      break
    case pageType = 'Blog':
      await search.waitUntilUrlContains(this.driver, 'blog')
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl, this.testUrl + '/blog', 'The blog page url is incorrect')
      break
    case pageType = 'About':
      await search.waitUntilUrlContains(this.driver, 'about')
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl, this.testUrl + '/about', 'The about page url is incorrect')
      break
    case pageType = 'Contact':
      await search.waitUntilUrlContains(this.driver, 'contact')
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl, this.testUrl + '/contact', 'The contact page url is incorrect')
      break
    case pageType = 'Features':
      await search.waitUntilUrlContains(this.driver, 'features')
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl, this.testUrl + '/features', 'The features page url is incorrect')
      break
    case pageType = 'Directory':
      await search.waitUntilUrlContains(this.driver, 'directory')
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl, this.testUrl + '/directory', 'The directory page url is incorrect')
      break
    case pageType = 'Funeral Directors A-Z':
      await search.waitUntilUrlContains(this.driver, 'a-z/a')
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl, this.testUrl + '/a-z/a', 'The funeral directors a-z page url is incorrect')
      break
    case pageType = 'Privacy':
      await search.waitUntilUrlContains(this.driver, 'privacy-policy')
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl, this.testUrl + '/privacy-policy', 'The privacy policy page url is incorrect')
      break
    case pageType = 'Terms of Use':
      await search.waitUntilUrlContains(this.driver, 'terms-and-conditions')
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl, this.testUrl + '/terms-and-conditions', 'The terms of use page url is incorrect')
      break
    case pageType = 'Sitemap':
      await search.waitUntilUrlContains(this.driver, 'sitemap')
      currentUrl = await this.driver.getCurrentUrl()
      assert.strictEqual(currentUrl, this.testUrl + '/sitemap', 'The sitemap page url is incorrect')
      break
  }
})
