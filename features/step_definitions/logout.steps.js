const { Given } = require('cucumber')
const assert = require('chai').assert
const search = require('../functions/searchMethods')

Given('I log out of the dashboard', async function () {
  let elementCheck, currentUrl
  elementCheck = await search.checkItemExistsCss(this.driver, 'a[class=\'link-logout\']')
  if (elementCheck === true) {
    await search.clickElementCss(this.driver, 'a[class=\'link-logout\']')
  } else {
    await this.driver.get(this.testUrl + '/logout')
  }

  await search.simpleWaitCss(this.driver, 'a[class=\'link-login\']')
  currentUrl = await this.driver.getCurrentUrl()
  assert.strictEqual(currentUrl, this.testUrl + '/logout', 'Logout was not successful.')
})
