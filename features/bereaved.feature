Feature: Bereaved user tests
These tests are to cover the elements that a bereaved user would use

    @setup @cbu
    Scenario: Create a bereaved user successfully
        Given I am logged in as admin
        And I have logged in as user "193919"
        And I am on the create obituary page
        When I create an obituary with the bereaved email address "bereaveduser@test.com"
        And I am logged in as admin
        Then I can log into the "bereaveduser@test.com" account

    @regression @bereaved
    Scenario: Bereaved client can leave review
        Given I am logged in as admin
        And I have impersonated user "bereaveduser@test.com"
        And I have clicked the leave review button on the bereaved dashboard
        When I complete the leave review process
        Then The review should be successfully sent

    @regression @bereaved
    Scenario: Bereaved client can edit loved ones details
        Given I am logged in as admin
        And I have impersonated user "bereaveduser@test.com"
        When I edit the loved ones details
        Then The bereaved edit is successfully saved
