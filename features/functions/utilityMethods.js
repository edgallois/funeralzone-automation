const me = require('user-home')
const os = require('os')
const fs = require('fs')

module.exports = {
  generateId: async function (len) {
    let id

    String.random = function (length) {
      let random13chars = function () {
        return Math.random().toString(16).substring(2, 15)
      }
      let loops = Math.ceil(length / 13)
      return new Array(loops).fill(random13chars).reduce((string, func) => {
        return string + func()
      }, '').substring(0, length)
    }
    id = String.random(len)

    return id
  },

  genDateTime: async function () {
    const date = new Date().toISOString().split('.')[0] + 'Z'
    return date
  },

  raffle: async function (type) {
    const services = ['DISBURSEMENTS', 'VEHICLES', 'OFFICIANTS', 'SERVICE_VENUES', 'CREMATORIA', 'PROFESSIONAL_SERVICES', 'OTHER', 'CEMETERIES']
    const products = ['COFFINS', 'FLOWERS', 'URNS', 'OTHER']
    const possessions = ['BRACELET', 'EARRING', 'NECKLACE', 'RING', 'WATCH', 'OTHER']
    const noteCategory = ['DECEASED', 'CLIENT', 'PACKAGES', 'CARE', 'VENUE', 'CORTEGE', 'COFFIN', 'URN', 'OFFICIANT', 'MUSIC', 'FLOWERS', 'OTHER_SERVICES', 'CHARITY', 'OBITUARY', 'OTHER']

    if (type === 'service') {
      return services[Math.floor(Math.random() * services.length)].toUpperCase()
    } else if (type === 'product') {
      return products[Math.floor(Math.random() * products.length)].toUpperCase()
    } else if (type === 'possession') {
      return possessions[Math.floor(Math.random() * products.length)].toUpperCase()
    } else if (type === 'note') {
      return noteCategory[Math.floor(Math.random() * products.length)].toUpperCase()
    }
  },

  scrollElementIntoView: async function (driver, webElement) {
    await driver.executeScript('arguments[0].scrollIntoView(true)', webElement)
  },

  currentUsername: function () {
    const user = me
    return user
  },

  fullDate: function () {
    const monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
      'July', 'August', 'September', 'October', 'November', 'December'
    ]
    const d = new Date()
    const day = d.getDate()
    const mon = monthNames[d.getMonth()]
    const yr = d.getFullYear()

    const date = day + '-' + mon + '-' + yr
    date.toString()
    return date
  },

  getTimestamp: function () {
    const randomNumber = Math.round(new Date().valueOf() / 1)
    return randomNumber
  },

  getRandomNumber: function (max) {
    return Math.floor(Math.random() * Math.floor(max))
  },

  getNumberBetweenTwoValues: function (min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min)
  },

  getFirstOfNextMonth: function () {
    const todayDate = new Date()
    let year = todayDate.getFullYear()
    let month = todayDate.getMonth() + 2 // To move forward one month
    if (month.toString().length < 2) { month = '0' + month }
    if (month > 12) {
      month = '01'
      year++
    }
    const firstOfNextMonth = year + '-' + month + '-01'
    return firstOfNextMonth
  },

  extractNumbersFromString: function (inString) {
    const noChars = inString.replace(/[^0-9]/g, '')
    return noChars
  },

  convertIntegerFromString: function (inString) {
    const noChars = inString.replace(/[^0-9]/g, '')
    const num = parseInt(noChars)
    return num
  },

  extractDecimalFromString: function (inString) {
    const noChars = inString.replace(/[^0-9\.]/g, '') // eslint-disable-line
    const num = parseFloat(noChars)
    return num
  },

  currentSystem: function () {
    const currentOs = os.platform()
    return currentOs
  },

  currentSystemVersion: function () {
    const sv = os.release()
    let systemVersion
    if (sv === '18.0.0') {
      // osx version
      systemVersion = '10.14'
    } else {
      // either linux or windows
      systemVersion = sv
    }
    return systemVersion
  },

  createFile: function (fileName) {
    fs.writeFileSync(process.cwd() + '/' + fileName)
  },

  readFile: function (fileLocation) {
    const fileData = fs.readFileSync(fileLocation)
    return fileData
  },

  sleep: function (milliseconds) {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
  }
}
