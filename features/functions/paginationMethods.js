const { By } = require('selenium-webdriver')
const utility = require('./utilityMethods')
const search = require('./searchMethods')
const assert = require('chai').assert

module.exports = {

  paginationSearchCss: async function (driver, titleList, itemToLookFor) {
    let titles = await search.waitForElementsCss(driver, titleList)
    let booleanResult = false
    let i = 0
    let row = i
    let resultItems = [
      booleanResult,
      row
    ]

    while (i < titles.length) {
      let thisTitle = await titles[i]
      await utility.scrollElementIntoView(driver, thisTitle)
      let currentTitle = await thisTitle.getText()
      let result = currentTitle.includes(itemToLookFor)

      if (result === true) {
        resultItems = [
          booleanResult = true,
          row = i
        ]
        break
      }
      i++
    }

    return resultItems
  },

  paginationSearchId: async function (driver, titleList, itemToLookFor) {
    let titles = await search.waitForElementsId(driver, titleList)
    let booleanResult = false
    let i = 0
    let row = i
    let resultItems = [
      booleanResult,
      row
    ]

    while (i < titles.length) {
      let thisTitle = await titles[i]
      await utility.scrollElementIntoView(driver, thisTitle)
      let currentTitle = await thisTitle.getText()
      let result = currentTitle.includes(itemToLookFor)

      if (result === true) {
        resultItems = [
          booleanResult = true,
          row = i
        ]
        break
      }
      i++
    }

    return resultItems
  },

  paginationAttributeSearchCss: async function (driver, titleList, attribute, itemToLookFor) {
    let titles = await search.waitForElementsCss(driver, titleList)
    let booleanResult = false
    let i = 0
    let row = i
    let resultItems = [
      booleanResult,
      row
    ]

    while (i < titles.length) {
      let thisTitle = titles[i]
      await utility.scrollElementIntoView(driver, thisTitle)
      let currentTitle = await thisTitle.getAttribute(attribute)
      let result = currentTitle.includes(itemToLookFor)

      if (result === true) {
        resultItems = [
          booleanResult = true,
          row = i
        ]
        break
      }
      i++
    }

    return resultItems
  },

  paginationAttributeSearchId: async function (driver, titleList, attribute, itemToLookFor) {
    let titles = await search.waitForElementsId(driver, titleList)
    let booleanResult = false
    let i = 0
    let row = i
    let resultItems = [
      booleanResult,
      row
    ]

    while (i < titles.length) {
      let thisTitle = titles[i]
      await utility.scrollElementIntoView(driver, thisTitle)
      let currentTitle = await thisTitle.getAttribute(attribute)
      let result = currentTitle.includes(itemToLookFor)

      if (result === true) {
        resultItems = [
          booleanResult = true,
          row = i
        ]
        break
      }
      i++
    }

    return resultItems
  },

  getPageNumberTotalId: async function (driver, elementLocator) {
    let pageElement = await driver.findElement(By.id(elementLocator)).getText()
    let totalPages = pageElement.substr(-3)
    let pageTotal = await utility.convertIntegerFromString(totalPages)

    return pageTotal
  },

  getPageNumberTotalCss: async function (driver, elementLocator) {
    let pageElement = await driver.findElement(By.css(elementLocator)).getText()
    let totalPages = pageElement.substr(-3)
    let pageTotal = await utility.convertIntegerFromString(totalPages)

    return pageTotal
  },

  clickNextPage: async function (driver, webElement) {
    let nextPage = await search.waitForElementId(driver, webElement)
    await utility.scrollElementIntoView(driver, nextPage)
    let pageAttribute = await nextPage.getAttribute('tabindex')
    let pageResult = pageAttribute.includes('-1')
    if (pageResult === true) {
      throw new Error('The item was not found after checking all pages')
    }
    await nextPage.click()
    await driver.sleep(700)
  },

  clickNextPageDelete: async function (driver, webElement) {
    let nextPage = await search.waitForElementId(driver, webElement)
    await utility.scrollElementIntoView(driver, nextPage)
    let pageAttribute = await nextPage.getAttribute('tabindex')
    let pageResult = pageAttribute.includes('-1')
    if (pageResult === true) {
      assert.isTrue(pageResult, 'The deleted item was found in error')
    } else if (pageResult === false) {
      await nextPage.click()
      await driver.sleep(700)
    }
  }
}
