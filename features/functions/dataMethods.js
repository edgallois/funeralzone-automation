const search = require('./searchMethods')
const utility = require('./utilityMethods')

module.exports = {
  enterTextInElementCss: async function (driver, cssLocator, requiredText) {
    let element = await search.findElementCss(driver, cssLocator)
    await utility.scrollElementIntoView(driver, element)
    await element.clear()
    await element.sendKeys(requiredText)
  },

  enterTextInElementId: async function (driver, idLocator, requiredText) {
    let element = await search.findElementId(driver, idLocator)
    await utility.scrollElementIntoView(driver, element)
    await element.clear()
    await element.sendKeys(requiredText)
  },

  enterTextInElement: async function (driver, element, requiredText) {
    await utility.scrollElementIntoView(driver, element)
    await element.clear()
    await element.sendKeys(requiredText)
  },

  getTextFromElementCss: async function (driver, cssLocator) {
    let cssElement = await search.findElementCss(driver, cssLocator)
    let text = await cssElement.getText()
    return text
  },

  getTextFromElementId: async function (driver, idLocator) {
    let idElement = await search.findElementId(driver, idLocator)
    let text = await idElement.getText()
    return text
  },

  getAttributeFromElementCss: async function (driver, cssLocator, attribute) {
    let cssElement = await search.findElementCss(driver, cssLocator)
    let elementAttribute = await cssElement.getAttribute(attribute)
    return elementAttribute
  },

  getAttributeFromElementId: async function (driver, idLocator, attribute) {
    let idElement = await search.findElementId(driver, idLocator)
    let elementAttribute = await idElement.getAttribute(attribute)
    return elementAttribute
  },

  getAttributeFromWebElement: async function (driver, webElement, attribute) {
    await utility.scrollElementIntoView(driver, webElement)
    let elementAttribute = await webElement.getAttribute(attribute)
    return elementAttribute
  }
}
