const utility = require('./utilityMethods')
const search = require('./searchMethods')

module.exports = {

  clickModalSaveButtonId: async function (driver) {
    const saveButtons = await search.findElementsId(driver, 'save')
    const btnLength = saveButtons.length
    if (btnLength > 1) {
      await utility.scrollElementIntoView(driver, saveButtons[1])
      await saveButtons[1].click()
    } else {
      await utility.scrollElementIntoView(driver, saveButtons[0])
      await saveButtons[0].click()
    }
  },

  clickModalSaveIdentityButtonId: async function (driver) {
    const saveButtons = await search.findElementsId(driver, 'saveIdentity')
    const btnLength = saveButtons.length
    if (btnLength > 1) {
      await utility.scrollElementIntoView(driver, saveButtons[1])
      await saveButtons[1].click()
    } else {
      await utility.scrollElementIntoView(driver, saveButtons[0])
      await saveButtons[0].click()
    }
  }
}
