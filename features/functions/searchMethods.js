const { By, until } = require('selenium-webdriver')
const utility = require('../functions/utilityMethods')

module.exports = {

  elementInParentContainsString: async function (driver, parentLocator, itemsLocator, itemToLookFor) {
    const getParentElement = await this.waitForElementCss(driver, parentLocator)
    await utility.scrollElementIntoView(driver, getParentElement)
    const getItemNames = await getParentElement.findElements(By.css(itemsLocator))
    let booleanResult = false
    let fullItemName
    let i = 0
    let row = i
    let resultItems = [booleanResult, row, fullItemName]
    let thisName
    let result
    while (i < getItemNames.length) {
      thisName = await getItemNames[i].getText()
      result = await thisName.includes(itemToLookFor)
      if (result === true) {
        resultItems = [booleanResult = true,
          row = i,
          fullItemName = thisName]
        return resultItems
      }
      i++
    }
    return resultItems
  },

  elementContainsString: async function (driver, itemsLocator, itemToLookFor) {
    await this.simpleWaitCss(driver, itemsLocator)
    const getItemNames = await this.findElementsCss(driver, itemsLocator)
    let booleanResult = false
    let fullItemName
    let i = 0
    let row = i
    let resultItems = [booleanResult, row, fullItemName]
    let thisName
    let result
    while (i < getItemNames.length) {
      thisName = await getItemNames[i].getText()
      result = await thisName.includes(itemToLookFor)
      if (result === true) {
        resultItems = [booleanResult = true,
          row = i,
          fullItemName = thisName]
        return resultItems
      }
      i++
    }
    return resultItems
  },

  checkForExcludedItemsCss: async function (driver, cssElementsLocator, stringsToLookFor) {
    const items = await this.waitForElementsCss(driver, cssElementsLocator)
    let booleanResult = true
    let i = 0
    let row = i
    let currentItem
    let excludedCheck
    let resultItems = [booleanResult, currentItem, row]
    while (i < items.length) {
      currentItem = await items[i].getText()
      excludedCheck = this.containsAny(currentItem, stringsToLookFor)
      if (excludedCheck === false) {
        resultItems = [
          booleanResult = false,
          currentItem,
          row = i
        ]
        return resultItems
      }
      i++
    }
    return resultItems
  },

  containsAny: function (str, substrings) {
    for (let i = 0; i !== substrings.length; i++) {
      let substring = substrings[i]
      if (str === substring) {
        return true
      }
    }
    return false
  },

  checkItemExistsCss: async function (driver, elementLocator) {
    try {
      await this.findElementCss(driver, elementLocator)
      return true
    } catch (err) {
      return false
    }
  },

  checkItemExistsId: async function (driver, elementLocator) {
    try {
      await this.findElementId(driver, elementLocator)
      return true
    } catch (err) {
      return false
    }
  },

  checkElementExists: async function (element) {
    try {
      await element.isEnabled
      return true
    } catch (err) {
      return false
    }
  },

  findElementCss: async function (driver, cssLocator) {
    const cssElement = await driver.findElement(By.css(cssLocator))
    return cssElement
  },

  findElementId: async function (driver, idLocator) {
    const idElement = await driver.findElement(By.id(idLocator))
    return idElement
  },

  findElementTagName: async function (driver, tagName) {
    const element = await driver.findElement(By.tagName(tagName))
    return element
  },

  findElementsCss: async function (driver, cssLocator) {
    const cssElements = await driver.findElements(By.css(cssLocator))
    return cssElements
  },

  findElementsId: async function (driver, idLocator) {
    const idElements = await driver.findElements(By.id(idLocator))
    return idElements
  },

  waitForElementCss: async function (driver, cssLocator) {
    const cssElement = await driver.wait(until.elementLocated(By.css(cssLocator)))
    return cssElement
  },

  waitForElementId: async function (driver, idLocator) {
    const idElement = await driver.wait(until.elementLocated(By.id(idLocator)))
    return idElement
  },

  waitForElementsCss: async function (driver, cssLocator) {
    const cssElements = await driver.wait(until.elementsLocated(By.css(cssLocator)))
    return cssElements
  },

  waitForElementsId: async function (driver, idLocator) {
    const idElements = await driver.wait(until.elementsLocated(By.id(idLocator)))
    return idElements
  },

  simpleWaitCss: async function (driver, cssLocator) {
    await driver.wait(until.elementLocated(By.css(cssLocator)))
  },

  simpleWaitId: async function (driver, idLocator) {
    await driver.wait(until.elementLocated(By.id(idLocator)))
  },

  waitForElementIsVisible: async function (driver, webElement) {
    await driver.wait(until.elementIsVisible(webElement))
  },

  waitForElementNotVisible: async function (driver, webElement) {
    await driver.wait(until.elementIsNotVisible(webElement))
  },

  waitForElementSelected: async function (driver, webElement) {
    await driver.wait(until.elementIsSelected(webElement))
  },

  waitUntilUrlContains: async function (driver, urlSegment) {
    await driver.wait(until.urlContains(urlSegment))
  },

  waitUntilUrlIs: async function (driver, fullUrl) {
    await driver.wait(until.urlIs(fullUrl))
  },

  clickElementCss: async function (driver, cssLocator) {
    const categorySelection = await this.findElementCss(driver, cssLocator)
    await utility.scrollElementIntoView(driver, categorySelection)
    await categorySelection.click()
  },

  clickElementId: async function (driver, idLocator) {
    const element = await this.findElementId(driver, idLocator)
    await utility.scrollElementIntoView(driver, element)
    await element.click()
  }
}
