const { After, Before, setDefaultTimeout } = require('cucumber')
const webdriver = require('selenium-webdriver')
const chrome = require('selenium-webdriver/chrome')
require('dotenv').config()
setDefaultTimeout(30 * 1000)

Before(function () {
  switch (process.env.ENVIRONMENT) {
    case process.env.ENVIRONMENT = 'local':
      this.testUrl = 'funeralzone.test'
      break
    case process.env.ENVIRONMENT = 'qa':
      this.testUrl = 'https://www.funeralzone.xyz'
      this.graphQlApiUrl = '' // TBC
      break
  }

  if (process.env.ENDPOINT_TYPE === 'website') {
    // Browser setup
    if (process.env.BROWSER === 'chrome') {
      require('chromedriver')
      let options = new chrome.Options()
      options.addArguments('disable-infobars', 'test-type')
      this.driver = new webdriver.Builder()
        .withCapabilities(options)
        .build()
      return this.driver
    } else if (process.env.BROWSER === 'firefox') {
      require('geckodriver')
      this.driver = new webdriver.Builder()
        .withCapabilities({
          'browserName': 'firefox',
          'moz:webdriverClick': false,
          'marionette': true
        })
        .build()
      return this.driver
    }
  } else if (process.env.ENDPOINT_TYPE === 'graphQlApi') {
    const GraphQlDriver = require('../../drivers/graphQlDriver')
    this.driver = new GraphQlDriver(this.graphQlApiUrl, process.env.ACCESS_TOKEN)
    this.driver.init()
    return this.driver
  }
})

After(function () {
  // end the session if still active
  return this.driver.quit()
})
