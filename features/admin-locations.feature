Feature: Admin user can add new locations edit existing locations and delete obsolete locations
Admin users now have the ability to add new locations quickly and easily
from within the admin dashboard.  This ability will enable admin users to make quick changes
to existing towns and counties (and states in the US) to fix errors that are reported.
Admin users can also delete locations if required.

    @adminlocations
    Scenario: Manage area types button functions correctly
        Given I am logged in as admin
        And I am on the admin > directory > "Locations" page
        When I click the "Manage Area Types" button on the locations page
        Then The area types page loads successfully

    @adminlocations
    Scenario: Add new location button functions correctly
        Given I am logged in as admin
        And I am on the admin > directory > "Locations" page
        When I click the "Add New Location" button on the locations page
        Then The add new location page loads successfully

    @adminlocations
    Scenario: Edit county location name successfully
        Given I am logged in as admin
        And I am on the admin > directory > "Locations" page
        And I click the "Manage Area Types" button on the locations page
        When I click the edit button on the area types page for a "county"
        Then I can successfully make changes to the "county" area type "name"

    @adminlocations
    Scenario: Edit town location name successfully
        Given I am logged in as admin
        And I am on the admin > directory > "Locations" page
        And I click the "Manage Area Types" button on the locations page
        When I click the edit button on the area types page for a "town"
        Then I can successfully make changes to the "town" area type "name"

    @adminlocations
    Scenario: Edit county location slug successfully
        Given I am logged in as admin
        And I am on the admin > directory > "Locations" page
        And I click the "Manage Area Types" button on the locations page
        When I click the edit button on the area types page for a "county"
        Then I can successfully make changes to the "county" area type "slug"

    @adminlocations
    Scenario: Edit town location slug successfully
        Given I am logged in as admin
        And I am on the admin > directory > "Locations" page
        And I click the "Manage Area Types" button on the locations page
        When I click the edit button on the area types page for a "town"
        Then I can successfully make changes to the "town" area type "slug"

    @adminlocations
    Scenario: Create new town location
        Given I am logged in as admin
        And I am on the admin > directory > "Locations" page
        And I click the "Add New Location" button on the locations page
        When I complete the add location flow for a new "town" location
        Then The new "town" location is successfully created

    @adminlocations
    Scenario: Create new county location
        Given I am logged in as admin
        And I am on the admin > directory > "Locations" page
        And I click the "Add New Location" button on the locations page
        When I complete the add location flow for a new "county" location
        Then The new "county" location is successfully created

    @adminlocations
    Scenario: Edit a town name
        Given I am logged in as admin
        And I am on the admin > directory > "Locations" page
        When I click the edit button next to a "town"
        Then I can edit the "town" name successfully

    @adminlocations
    Scenario: Edit a county name
        Given I am logged in as admin
        And I am on the admin > directory > "Locations" page
        When I click the edit button next to a "county"
        Then I can edit the "county" name successfully
