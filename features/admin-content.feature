Feature: Admin user can view and edit website content for Funeral Zone
Admin users have the ability to add and edit website content throughout
The Funeral Zone Scenario Outline: 

    @ac
    Scenario: Admin user can set a poem as a featured poem
        Given I am logged in as admin
        And I am on the admin > content > website pages > information pages > "poems" page
        And I select a "poem" from the available list
        When I mark the item as "featured"
        Then The featured "poem" is displayed on the page
    
    @ac
    Scenario: Admin user can set a hymn as a featured hymn
        Given I am logged in as admin
        And I am on the admin > content > website pages > information pages > "hymns" page
        And I select a "hymn" from the available list
        When I mark the item as "featured"
        Then The featured "hymn" is displayed on the page
    
    @ac
    Scenario: Admin user can remove a poem as featured
        Given I am logged in as admin
        And I am on the admin > content > website pages > information pages > "poems" page
        And I select a previously featured "poem" from the list
        When I mark the item as "not featured"
        Then The "poem" is not featured on the page
    
    @ac
    Scenario: Admin user can remove a hymn as featured
        Given I am logged in as admin
        And I am on the admin > content > website pages > information pages > "hymns" page
        And I select a previously featured "hymn" from the list
        When I mark the item as "not featured"
        Then The "hymn" is not featured on the page
        