Feature: Provide visibilty and billing of First Calls to Partners and Members
Each first call is a call that is made to our partners and members
through the Funeral Zone website
that would potentially lead to a new Funeral

Whilst billing is not available, for testing please set the first_calls.charge flag to true

Clients being used are (which are subject to change):
Enabled with card - Murray's Independent Funeral Directors
Enabled with direct debit - J. Vernon Kendrick Funeral Directors
Enabled with invoice - A W Lymn
Enabled with Free - Dignity UK

    @firstcall
    Scenario: Client with first calls enabled with payment type of unlimited card has calls displayed
        Given I am logged in as admin
        When I have logged in as user "1783"
        Then I see a "First Calls" header displayed

    @firstcall
    Scenario: Client with first calls enabled with payment type of unlimited direct debit has calls displayed
        Given I am logged in as admin
        When I have logged in as user "82012"
        Then I see a "First Calls" header displayed

    @firstcall
    Scenario: A client with first calls enabled with payment type of unlimited invoice does not see first calls listed
        Given I am logged in as admin
        When I have logged in as user "110586"
        Then I see a "Stats/Reviews" header displayed

    @firstcall
    Scenario: A client with first calls enabled but with a free account type does not see first calls listed
        Given I am logged in as admin
        And I have logged in as user "133525"
        Then I see a "Stats/Reviews" header displayed

    @fcdispute
    Scenario: Client can dispute a first call
        Given I am logged in as admin
        And I have logged in as user "82012"
        When I dispute a first call that is displayed
        Then The disputed call is now shown as in dispute

    @fcdispute
    Scenario: Dispute resolved status updated
        Given I am logged in as admin
        And I have logged in as user "82012"
        And I have the company name to search for
        And I am on the phone calls page
        And I have searched for the company
        When I resolve an open first call dispute on the admin dashboard
        And I have logged in as user "82012"
        Then The call is marked as "Dispute Resolved" on the client dashboard

    @callfilter
    Scenario: Can export a csv file of calls
        Given I am logged in as admin
        And I have logged in as user "110586"
        And I have the company name to search for
        And I am on the phone calls page
        When I filter the calls by the company and the call type to "First Call"
        And I click the download filtered results button
        Then A csv file should be downloaded