Feature: Admin user can edit charity details in the admin dashboard
    Admin users have the ability to view and add contacts to the charities on the 
    Funeral Zone site.  These tests cover the functions available to admin users
    who use the admin dashboard for this purpose

    @adminfunctions @admincharities
    Scenario: Admin user can add a contact to a charity
        Given I am logged in as admin
        And I am on the admin > charities page
        When I select a charity from the list
        And I add a contact to the charity
        Then The contact is added successfully to the charity