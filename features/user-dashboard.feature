Feature: User dashboard functionality
These tests cover the functionality of the various aspects and navigation of
the user dashboard

    @regression @dashboard
    Scenario: Master user can view their company reviews
        Given I am logged in as admin
        And I have logged in as user "193919"
        When I click on the "reviews" button on the dashboard
        Then I am taken to the "reviews" page

    @regression @dashboard
    Scenario: Master user can access their obituary branding page
        Given I am logged in as admin
        And I have logged in as user "193919"
        When I click on the "configure obituaries" button on the dashboard
        Then I am taken to the "branding configuration" page

    @regression @dashboard
    Scenario: Master user can access their payment receipts page
        Given I am logged in as admin
        And I have logged in as user "193919"
        When I click on the "receipts" button on the dashboard
        Then I am taken to the "receipts" page

    @regression @dashboard
    Scenario: Master user can access their email settings page
        Given I am logged in as admin
        And I have logged in as user "193919"
        When I click on the "email setting" button on the dashboard
        Then I am taken to the "email settings" page

    @reviews @rrr
    Scenario: Master user can reply to reviews on their dashboard
        Given I am logged in as admin
        And I have logged in as user "193919"
        And I click on the "reviews" button on the dashboard
        When I click the reply button on a review
        Then I can leave a reply to a review

    @reviews
    Scenario: Master user can edit a reply on a review
        Given I am logged in as admin
        And I have logged in as user "193919"
        And I click on the "reviews" button on the dashboard
        When I click the edit button on a previously left review reply
        Then I can edit the reply to the review

    @reviews
    Scenario: Master user can delete a reply to a review
        Given I am logged in as admin
        And I have logged in as user "193919"
        And I click on the "reviews" button on the dashboard
        When I click the delete button on a previously left review reply
        Then The review reply is deleted
