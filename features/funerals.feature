Feature: Funeral Directors can create and maintain funerals on the FZ dashboard
As a Funeral Director user, they should be able to create and maintain funerals on the FZ platform

    @funerals @newfuneral
    Scenario: Create new funeral successfully
        Given I am logged in as admin
        And I have logged in as user "193919"
        And I am on the add funeral page
        When I complete the new funeral form without any errors
        Then The funeral is successfully created and I am taken to a confirmation page

    @funerals @newfuneral
    Scenario: Duplicate funeral validation check
        Given I am logged in as admin
        And I have logged in as user "193919"
        And I am on the add funeral page
        When I complete the new funeral form with existing funeral details
        Then There should be validation errors displayed on the page about a duplicate funeral

    @funerals @newfuneral
    Scenario: Create new funeral required fields validation check - missing deceased name
        Given I am logged in as admin
        And I have logged in as user "193919"
        And I am on the add funeral page
        When I complete the new funeral form with missing "deceased name" details
        Then I should see a validation error message on the "deceased name" field

    @funerals @newfuneral
    Scenario: Create new funeral required fields validation check - missing date of death
        Given I am logged in as admin
        And I have logged in as user "193919"
        And I am on the add funeral page
        When I complete the new funeral form with missing "dod" details
        Then I should see a validation error message on the "dod" field

    @funerals @newfuneral
    Scenario: Create new funeral required fields validation check - missing client name
        Given I am logged in as admin
        And I have logged in as user "193919"
        And I am on the add funeral page
        When I complete the new funeral form with missing "client name" details
        Then I should see a validation error message on the "client name" field

    @funerals @newfuneral
    Scenario: Create new funeral required fields validation check - missing client contact details
        Given I am logged in as admin
        And I have logged in as user "193919"
        And I am on the add funeral page
        When I complete the new funeral form with missing "client contact" details
        Then I should see a validation error message on the "client contact" field
