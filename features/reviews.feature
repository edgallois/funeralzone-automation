Feature: Funeral Directors can view, reply and configure their client reviews
As a Funeral Director user
I expect to have an area of my dashboard
where I can view, reply and filter reviews left by clients

    @reviews @rr
    Scenario: Funeral Director can reply to a review
        Given I am logged in as admin
        And I have logged in as user "193919"
        When I am on the reviews page
        Then I can reply successfully to a listed review

    @reviews
    Scenario: Funeral Director can choose which review notifications they receive by star rating
        Given I am logged in as admin
        And I have logged in as user "193919"
        When I am on the reviews page
        Then I can change which email notifications I receive based on the star rating of the review
