Feature: Admin user can view and manage clients and calls from the admin dashboard
    Admin users have access to the various companies and calls that are received on the
    admin dashboard. These tests are to ensure that admin users can use the tools they
    require on this section of the admin dashboard.

    @adminfunctions @adminnewcompany
    Scenario: Admin user can add a new company
        Given I am logged in as admin
        And I am on the clients > companies page
        And I click the "add new company" button on the companies page
        When I complete the add new company form
        Then The new company is successfully created

    @adminfunctions @adminclients
    Scenario: Admin user can add a brand to a company
        Given I am logged in as admin
        And I am on the clients > companies page
        And I click the "add brand" button on the companies page
        When I add a brand to the company
        Then The brand is saved successfully

    @adminfunctions @adminclients
    Scenario: Admin user can edit a company
        Given I am logged in as admin
        And I am on the clients > companies page
        And I click the "edit" button on the companies page
        When I edit the company details
        Then The edited company is saved successfully

    @adminfunctions @adminclients
    Scenario: Admin user can add company funeral numbers
        Given I am logged in as admin
        And I am on the clients > companies page
        And I click the "funeral numbers" button on the companies page
        When I upload a file will funeral numbers
        Then The funeral numbers for the company are updated

    @adminfunctions @adminclients
    Scenario: Admin user can change company features
        Given I am logged in as admin
        And I am on the clients > companies page
        And I click the "manage features" button on the companies page
        When I change the selected features available to a company
        Then The selected features are saved successfully

    @adminfunctions @adminclients @pop
    Scenario: Admin user can manage popular services for a company
        Given I am logged in as admin
        And I am on the clients > companies page
        And I click the "manage popular services" button on the companies page
        When I change the available popular services
        Then The selected popular features are saved successfully

    @adminfunctions @adminclients
    Scenario: Admin user can add a bulk upload to a company
        Given I am logged in as admin
        And I am on the clients > companies page
        And I click the "manage bulk import" button on the companies page
        When I add a new bulk import reference
        Then The bulk import is successfully saved

    @adminclients @clientreset
    Scenario: Reset tested company to default values
        Given I am logged in as admin
        When I reset the test values to default settings
        Then The reset is successful