Feature: Simple Funeral Zone website checks to ensure pages are being served correctly
As we have a large number of pages on the FZ site, these simple tests will
check the main pages are being returned in a timely manner, and could be added as part of
a sanity check going forward

    @website @header @hd
    Scenario: Compare funeral directors page loads successfully
        Given I am on the Funeral Zone homepage
        When I click the "Compare Funeral Directors" link in the navigation bar
        Then The "Funeral Directors" page should load successfully

    @website @header
    Scenario: Help & Resources page loads successfully
        Given I am on the Funeral Zone homepage
        When I click the "Help & Resources" link in the navigation bar
        Then The "Help & Resources" page should load successfully

    @website @header
    Scenario: Obituaries page loads successfully
        Given I am on the Funeral Zone homepage
        When I click the "Obituaries" link in the navigation bar
        Then The "Obituaries" page should load successfully

    @website @header
    Scenario: Funeral Wishes page loads successfully
        Given I am on the Funeral Zone homepage
        When I click the "Funeral Wishes" link in the navigation bar
        Then The "Funeral Wishes" page should load successfully

    @website @header
    Scenario: Funeral Plans page loads successfully
        Given I am on the Funeral Zone homepage
        When I click the "Funeral Plans" link in the navigation bar
        Then The "Funeral Plans" page should load successfully

    @website @header
    Scenario: Funeral Directors - Join Now page loads successfully
        Given I am on the Funeral Zone homepage
        When I click the "Funeral Directors - Join Now" link in the navigation bar
        Then The "Funeral Directors - Join Now" page should load successfully
    
    @website @header
    Scenario: Blogs page loads successfully - header
        Given I am on the Funeral Zone homepage
        When I click the "Blog" link in the navigation bar
        Then The "Blog" page should load successfully

    @website @footer
    Scenario: About page loads successfully
        Given I am on the Funeral Zone homepage
        When I click the "About" link in the footer
        Then The "About" page should load successfully
    
    @website @footer
    Scenario: Contact page loads successfully
        Given I am on the Funeral Zone homepage
        When I click the "Contact" link in the footer
        Then The "Contact" page should load successfully

    @website @footer
    Scenario: Blog page loads successfully - footer
        Given I am on the Funeral Zone homepage
        When I click the "Blog" link in the footer
        Then The "Blog" page should load successfully

    @website @footer
    Scenario: Features page loads successfully
        Given I am on the Funeral Zone homepage
        When I click the "Features" link in the footer
        Then The "Features" page should load successfully

    @website @footer
    Scenario: Directory page loads successfully
        Given I am on the Funeral Zone homepage
        When I click the "Directory" link in the footer
        Then The "Directory" page should load successfully

    # @website @footer @az
    # Scenario: Funeral Director A-Z page loads correctly
    #     Given I am on the Funeral Zone homepage
    #     When I click the "Funeral Directors A-Z" link in the footer
    #     Then The "Funeral Directors A-Z" page should load successfully

    @website @footer
    Scenario: Privacy page loads correctly
        Given I am on the Funeral Zone homepage
        When I click the "Privacy" link in the footer
        Then The "Privacy" page should load successfully
    
    @website @footer
    Scenario: Terms of Use page loads correctly
        Given I am on the Funeral Zone homepage
        When I click the "Terms of Use" link in the footer
        Then The "Terms of Use" page should load successfully
    
    @website @footer
    Scenario: Sitemap page loads correctly
        Given I am on the Funeral Zone homepage
        When I click the "Sitemap" link in the footer
        Then The "Sitemap" page should load successfully

    # @website @bpp
    # Scenario: Latest reviews should link to the business profile page they are linked to
    #     Given I am on a funeral directors listing page
    #     When I click a review on the page
    #     Then I am taken to the business profile page for the review

