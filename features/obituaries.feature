Feature: Funeral Directors can create and manage obituaries on the FZ dashboard
As a Funeral Director user, they should be able to create and maintain obituaries on the FZ platform.

    @obituary @newobit
    Scenario: Create new obituary successfully
        Given I am logged in as admin
        And I have logged in as user "193919"
        And I am on the create obituary page
        When I complete the obituary form without errors
        Then The obituary is successfully created and I am on the obituaries screen

    @obituary @partialdob
    Scenario: Create new funeral required fields validation check - partial date of birth validation
        Given I am logged in as admin
        And I have logged in as user "193919"
        And I am on the create obituary page
        When I complete the obituary form with partial dob details
        Then I see a partial date of birth validation error on the obituaries page

    @obituary @partialdod
    Scenario: Create new funeral required fields validation check - partial date of death validation
        Given I am logged in as admin
        And I have logged in as user "193919"
        And I am on the create obituary page
        When I complete the obituary form with partial dod details
        Then I see a partial date of death validation error on the obituaries page

    @obituary
    Scenario: Duplicate obituary validation check
        Given I am logged in as admin
        And I have logged in as user "193919"
        And I am on the create obituary page
        When I complete the obituary form with an existing funeral
        Then I see a duplicate user validation error on the form

    @obituary @obitemail
    Scenario: Create obituary strict validation check - email only
        Given I am logged in as admin
        And I have logged in as user "193919"
        And I am on the create obituary page
        When I complete the obituary form with missing contact details
        Then I see a contact details validation error on the form

    @regression @obituary @oe
    Scenario: Successful edit of obituary
        Given I am logged in as admin
        And I have logged in as user "193919"
        And I am on the My Obituaries page
        When I select an obituary to edit and make changes and save
        Then The obituary edit is saved successfully

    @regression @obituary
    Scenario: Add comment to obituary
        Given I am logged in as admin
        And I have logged in as user "193919"
        And I am on the My Obituaries page
        When I select an obituary to view
        Then I can successfully send a message on the obituary

    @regression @obituary
    Scenario: Add photo to obituary
        Given I am logged in as admin
        And I have logged in as user "193919"
        And I am on the My Obituaries page
        When I select an obituary to view
        Then I can successfully add a photo to the obituary

    @regression @obituary @candle
    Scenario: Light a candle on obituary
        Given I am logged in as admin
        And I have logged in as user "193919"
        And I am on the My Obituaries page
        When I select an obituary to view
        Then I can successfully light a candle on the obituary