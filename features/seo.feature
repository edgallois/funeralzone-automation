Feature: Update SEO features across the Funeral Zone website
As a business we want to increase our SEO optimisations
So we can drive more traffic to the site
This feature will be updated with additional SEO related scenario's as time progresses

    @canonical @seo
    Scenario: London location that has fewer than 3 Funeral Directors has correct canonical link reference
        Given I am on the Funeral Directors page for the area of "Mitcham"
        When I check the page source "body"
        Then I see a canonical link that links back to the "London" Funeral Directors page

    @canonical @seo
    Scenario: Location outside major city with fewer than 3 Funeral Directors has correct canonical link reference
        Given I am on the Funeral Directors page for the area of "South Molton"
        When I check the page source "body"
        Then I see a canonical link that links back to the "Main" Funeral Directors page

    @ga @seo
    Scenario: Google Analytics script should be enabled on the Funeral Zone website
        Given I am on the Funeral Zone homepage
        When I check the page source "body"
        Then I see the Google Analytics tracking script

    @funeralplans @seo
    Scenario: Submit a new enquiry from the Funeral Plans page successfully
        Given I am on the "funeral plans" page
        When I complete the new enquiry form "without errors"
        Then The form is submitted successfully

    @funeralplans @seo
    Scenario: Funeral Plans enquiry form validation
        Given I am on the "funeral plans" page
        When I complete the new enquiry form "with errors"
        Then I see an error message on the form

    @funeralplans @fpmeta @seo
    Scenario: Updated meta title available on the Funeral Plans page
        Given I am on the "funeral plans" page
        When I check the page source "body"
        Then I see the updated meta "title" for the Funeral Plans page

    @funeralplans @fpmeta @seo
    Scenario: Updated meta description available on the Funeral Plans page
        Given I am on the "funeral plans" page
        When I check the page source "body"
        Then I see the updated meta "description" for the Funeral Plans page

    @funeralplans @fpmeta @seo
    Scenario: Added Open Graph markup on the Funeral Plans page
        Given I am on the "funeral plans" page
        When I check the page source "body"
        Then I see the updated meta "open graph" for the Funeral Plans page

    @funeralplans @fpmeta @seo
    Scenario: Added Twitter Card markup on the Funeral Plans page
        Given I am on the "funeral plans" page
        When I check the page source "body"
        Then I see the updated meta "twitter card" for the Funeral Plans page

    @baw @seo
    Scenario: Bereavement at work main page title meta data is correct
        Given I am on the "bereavement at work" page
        When I check the page source "head"
        Then I see the bereavement at work "title" meta data

    @baw @seo
    Scenario: Bereavement at work main page description meta data is correct
        Given I am on the "bereavement at work" page
        When I check the page source "head"
        Then I see the bereavement at work "description" meta data

    @baw @seo
    Scenario: Bereavement at work Guidance for Employers page title meta data is correct
        Given I am on the "bereavement at work" page
        And I click the "guidance for employers" link
        When I check the page source "head"
        Then I see the guidance for employers "title" meta data

    @baw @seo
    Scenario: Bereavement at work Guidance for Employers page description meta data is correct
        Given I am on the "bereavement at work" page
        And I click the "guidance for employers" link
        When I check the page source "head"
        Then I see the guidance for employers "description" meta data

    @baw @seo
    Scenario: Bereavement at work Workplace Strategy page title meta data is correct
        Given I am on the "bereavement at work" page
        And I click the "workplace strategy" link
        When I check the page source "head"
        Then I see the workplace strategy "title" meta data

    @baw @seo
    Scenario: Bereavement at work Workplace Strategy page description meta data is correct
        Given I am on the "bereavement at work" page
        And I click the "workplace strategy" link
        When I check the page source "head"
        Then I see the workplace strategy "description" meta data

    @baw @seo
    Scenario: Bereavement at work Coping With Bereavement page title meta data is correct
        Given I am on the "bereavement at work" page
        And I click the "coping with bereavement" link
        When I check the page source "head"
        Then I see the coping with bereavement "title" meta data

    @baw @seo
    Scenario: Bereavement at work Coping With Bereavement page description meta data is correct
        Given I am on the "bereavement at work" page
        And I click the "coping with bereavement" link
        When I check the page source "head"
        Then I see the coping with bereavement "description" meta data

    @baw @seo
    Scenario: Bereavement at work Guide to Bereavement Leave page title meta data is correct
        Given I am on the "bereavement at work" page
        And I click the "guide to bereavement leave" link
        When I check the page source "head"
        Then I see the guide to bereavement leave "title" meta data

    @baw @seo
    Scenario: Bereavement at work Guide to Bereavement Leave page description meta data is correct
        Given I am on the "bereavement at work" page
        And I click the "guide to bereavement leave" link
        When I check the page source "head"
        Then I see the guide to bereavement leave "description" meta data

    @baw @seo
    Scenario: Bereavement at work Parental Bereavement Leave page title meta data is correct
        Given I am on the "bereavement at work" page
        And I click the "parental bereavement leave" link
        When I check the page source "head"
        Then I see the parental bereavement leave "title" meta data

    @baw @seo
    Scenario: Bereavement at work Parental Bereavement Leave page description meta data is correct
        Given I am on the "bereavement at work" page
        And I click the "parental bereavement leave" link
        When I check the page source "head"
        Then I see the parental bereavement leave "description" meta data

    @seo @dimensions @d4
    Scenario: Dimension4 tracking code is displayed on the Funeral Zone homepage
        Given I am on the Funeral Zone homepage
        When I check the page source "body"
        Then The "dimension4" tracking code is visible

    @seo @dimensions @d4
    Scenario: Dimension4 tracking code is displayed on the Funeral Directors page
        Given I am on the "funeral directors" page
        When I check the page source "body"
        Then The "dimension4" tracking code is visible

    @seo @dimensions @d4
    Scenario: Dimension4 tracking code is displayed on the Help & Resources page
        Given I am on the "help & resources" page
        When I check the page source "body"
        Then The "dimension4" tracking code is visible

    @seo @dimensions @d4
    Scenario: Dimension4 tracking code is displayed on the Obituaries page
        Given I am on the "obituaries" page
        When I check the page source "body"
        Then The "dimension4" tracking code is visible

    @seo @dimensions @d4
    Scenario: Dimension4 tracking code is displayed on the Funeral Wishes page
        Given I am on the "funeral wishes" page
        When I check the page source "body"
        Then The "dimension4" tracking code is visible

    @seo @dimensions @d4
    Scenario: Dimension4 tracking code is displayed on the Funeral Plans page
        Given I am on the "funeral plans" page
        When I check the page source "body"
        Then The "dimension4" tracking code is visible

    @seo @dimensions @d4
    Scenario: Dimension4 tracking code is displayed on the Blog page
        Given I am on the "blog" page
        When I check the page source "body"
        Then The "dimension4" tracking code is visible

    @seo @dimensions @d4
    Scenario: Dimension4 tracking code is displayed on the About page
        Given I am on the "about" page
        When I check the page source "body"
        Then The "dimension4" tracking code is visible

    @seo @dimensions @d4
    Scenario: Dimension4 tracking code is displayed on the Contact page
        Given I am on the "contact" page
        When I check the page source "body"
        Then The "dimension4" tracking code is visible

    @seo @dimensions @d4
    Scenario: Dimension4 tracking code is displayed on the Features page
        Given I am on the "features" page
        When I check the page source "body"
        Then The "dimension4" tracking code is visible

    @seo @dimensions @d4
    Scenario: Dimension4 tracking code is displayed on the Directory page
        Given I am on the "directory" page
        When I check the page source "body"
        Then The "dimension4" tracking code is visible

    @seo @dimensions @d4
    Scenario: Dimension4 tracking code is displayed on the Funeral Directors A-Z page
        Given I am on the "funeral directors a-z" page
        When I check the page source "body"
        Then The "dimension4" tracking code is visible

    @seo @dimensions @d4
    Scenario: Dimension4 tracking code is displayed on the Privacy Policy page
        Given I am on the "privacy policy" page
        When I check the page source "body"
        Then The "dimension4" tracking code is visible

    @seo @dimensions @d4
    Scenario: Dimension4 tracking code is displayed on the Terms & Conditions page
        Given I am on the "terms & conditions" page
        When I check the page source "body"
        Then The "dimension4" tracking code is visible

    @seo @dimensions @d4
    Scenario: Dimension4 tracking code is displayed on the Sitemap page
        Given I am on the "sitemap" page
        When I check the page source "body"
        Then The "dimension4" tracking code is visible

    @seo @dimensions @d4
    Scenario: Dimension4 tracking code is displayed on a Funeral Directors location page
        Given I am on the "london" page
        When I check the page source "body"
        Then The "dimension4" tracking code is visible

    @seo @dimensions @d4
    Scenario: Dimension4 tracking code is displayed on a Business Profile page
        Given I am on the "london" page
        And I select the top funeral director page that is listed
        When I check the page source "body"
        Then The "dimension4" tracking code is visible

    @seo @dimensions @d4
    Scenario: Dimension4 tracking code is displayed on an Obituary page
        Given I am on the obituaries page for Exeter
        And I select the first obituary displayed
        When I check the page source "body"
        Then The "dimension4" tracking code is visible

    @seo @dimensions @d6
    Scenario: Dimension6 tracking code is displayed on the Funeral Zone homepage
        Given I am on the Funeral Zone homepage
        When I check the page source "body"
        Then The "dimension6" tracking code is visible

    @seo @dimensions @d6
    Scenario: Dimension6 tracking code is displayed on the Funeral Directors page
        Given I am on the "funeral directors" page
        When I check the page source "body"
        Then The "dimension6" tracking code is visible

    @seo @dimensions @d6
    Scenario: Dimension6 tracking code is displayed on the Help & Resources page
        Given I am on the "help & resources" page
        When I check the page source "body"
        Then The "dimension4" tracking code is visible

    @seo @dimensions @d6
    Scenario: Dimension6 tracking code is displayed on the Obituaries page
        Given I am on the "obituaries" page
        When I check the page source "body"
        Then The "dimension6" tracking code is visible

    @seo @dimensions @d6
    Scenario: Dimension6 tracking code is displayed on the Funeral Wishes page
        Given I am on the "funeral wishes" page
        When I check the page source "body"
        Then The "dimension6" tracking code is visible

    @seo @dimensions @d6
    Scenario: Dimension6 tracking code is displayed on the Funeral Plans page
        Given I am on the "funeral plans" page
        When I check the page source "body"
        Then The "dimension6" tracking code is visible

    @seo @dimensions @d6
    Scenario: Dimension6 tracking code is displayed on the Blog page
        Given I am on the "blog" page
        When I check the page source "body"
        Then The "dimension6" tracking code is visible

    @seo @dimensions @d6
    Scenario: Dimension6 tracking code is displayed on the About page
        Given I am on the "about" page
        When I check the page source "body"
        Then The "dimension6" tracking code is visible

    @seo @dimensions @d6
    Scenario: Dimension6 tracking code is displayed on the Contact page
        Given I am on the "contact" page
        When I check the page source "body"
        Then The "dimension6" tracking code is visible

    @seo @dimensions @d6
    Scenario: Dimension6 tracking code is displayed on the Features page
        Given I am on the "features" page
        When I check the page source "body"
        Then The "dimension6" tracking code is visible

    @seo @dimensions @d6
    Scenario: Dimension6 tracking code is displayed on the Directory page
        Given I am on the "directory" page
        When I check the page source "body"
        Then The "dimension6" tracking code is visible

    @seo @dimensions @d6
    Scenario: Dimension6 tracking code is displayed on the Funeral Directors A-Z page
        Given I am on the "funeral directors a-z" page
        When I check the page source "body"
        Then The "dimension6" tracking code is visible

    @seo @dimensions @d6
    Scenario: Dimension6 tracking code is displayed on the Privacy Policy page
        Given I am on the "privacy policy" page
        When I check the page source "body"
        Then The "dimension6" tracking code is visible

    @seo @dimensions @d6
    Scenario: Dimension6 tracking code is displayed on the Terms & Conditions page
        Given I am on the "terms & conditions" page
        When I check the page source "body"
        Then The "dimension6" tracking code is visible

    @seo @dimensions @d6
    Scenario: Dimension6 tracking code is displayed on the Sitemap page
        Given I am on the "sitemap" page
        When I check the page source "body"
        Then The "dimension6" tracking code is visible

    @seo @dimensions @d6
    Scenario: Dimension6 tracking code is displayed on a Funeral Directors location page
        Given I am on the "london" page
        When I check the page source "body"
        Then The "dimension6" tracking code is visible

    @seo @dimensions @d6
    Scenario: Dimension6 tracking code is displayed on a Business Profile page
        Given I am on the "london" page
        And I select the top funeral director page that is listed
        When I check the page source "body"
        Then The "dimension6" tracking code is visible

    @seo @dimensions @d6
    Scenario: Dimension6 tracking code is displayed on an Obituary page
        Given I am on the obituaries page for Exeter
        And I select the first obituary displayed
        When I check the page source "body"
        Then The "dimension6" tracking code is visible

    @seo @dimensions @d1
    Scenario: Dimension1 tracking code is displayed on a Business Profile page
        Given I am on the "london" page
        And I select the top funeral director page that is listed
        When I check the page source "body"
        Then The "dimension1" tracking code is visible

    @seo @dimensions @d1
    Scenario: Dimension1 tracking code is displayed on a Business Profile page
        Given I am on the "london" page
        When I check the page source "body"
        Then The "dimension1" tracking code is visible

    @seo @dimensions @d1
    Scenario: Dimension1 tracking code is displayed on an Obituary page
        Given I am on the obituaries page for Exeter
        And I select the first obituary displayed
        When I check the page source "body"
        Then The "dimension1" tracking code is visible

    @seo @dimensions @d2
    Scenario: Dimension2 tracking code is displayed on a Business Profile page
        Given I am on the "london" page
        And I select the top funeral director page that is listed
        When I check the page source "body"
        Then The "dimension2" tracking code is visible

    @seo @dimensions @d2
    Scenario: Dimension2 tracking code is displayed on a Business Profile page
        Given I am on the "london" page
        When I check the page source "body"
        Then The "dimension2" tracking code is visible

    @seo @dimensions @d2
    Scenario: Dimension2 tracking code is displayed on an Obituary page
        Given I am on the obituaries page for Exeter
        And I select the first obituary displayed
        When I check the page source "body"
        Then The "dimension2" tracking code is visible

    @seo @dimensions @d5
    Scenario: Dimension5 tracking code is not displayed on the Funeral Zone homepage if user is not logged in
        Given I am on the Funeral Zone homepage
        When I check the page source "body"
        Then The dimension5 tracking code is not visible

    @seo @dimensions @d5
    Scenario: Dimension5 tracking code is not displayed on the Obituaries page if user is not logged in
        Given I am on the "obituaries" page
        When I check the page source "body"
        Then The dimension5 tracking code is not visible

    @seo @dimensions @d5
    Scenario: Dimension5 tracking code is not displayed on the Funeral Wishes page if the user is not logged in
        Given I am on the "funeral wishes" page
        When I check the page source "body"
        Then The dimension5 tracking code is not visible

    @seo @dimensions @d5
    Scenario: Dimension5 tracking code is not displayed on the Funeral Plans page if the user is not logged in
        Given I am on the "funeral plans" page
        When I check the page source "body"
        Then The dimension5 tracking code is not visible

    @seo @dimensions @d5
    Scenario: Dimension5 tracking code is not displayed on the Blog page if the user is not logged in
        Given I am on the "blog" page
        When I check the page source "body"
        Then The dimension5 tracking code is not visible

    @seo @dimensions @d5
    Scenario: Dimension5 tracking code is not displayed on the About page if the user is not logged in
        Given I am on the "about" page
        When I check the page source "body"
        Then The dimension5 tracking code is not visible

    @seo @dimensions @d5
    Scenario: Dimension5 tracking code is not displayed on the Contact page if the user is not logged in
        Given I am on the "contact" page
        When I check the page source "body"
        Then The dimension5 tracking code is not visible

    @seo @dimensions @d5
    Scenario: Dimension5 tracking code is not displayed on the Features page if the user is not logged in
        Given I am on the "features" page
        When I check the page source "body"
        Then The dimension5 tracking code is not visible

    @seo @dimensions @d5
    Scenario: Dimension5 tracking code is not displayed on the Directory page if the user is not logged in
        Given I am on the "directory" page
        When I check the page source "body"
        Then The dimension5 tracking code is not visible

    @seo @dimensions @d5
    Scenario: Dimension5 tracking code is not displayed on the Funeral Directors A-Z page if the user is not logged in
        Given I am on the "funeral directors a-z" page
        When I check the page source "body"
        Then The dimension5 tracking code is not visible

    @seo @dimensions @d5
    Scenario: Dimension5 tracking code is not displayed on the Privacy Policy page if the user is not logged in
        Given I am on the "privacy policy" page
        When I check the page source "body"
        Then The dimension5 tracking code is not visible

    @seo @dimensions @d5
    Scenario: Dimension5 tracking code is not displayed on the Terms & Conditions page if the user is not logged in
        Given I am on the "terms & conditions" page
        When I check the page source "body"
        Then The dimension5 tracking code is not visible

    @seo @dimensions @d5
    Scenario: Dimension5 tracking code is not displayed on the Sitemap page if the user if not logged in
        Given I am on the "sitemap" page
        When I check the page source "body"
        Then The dimension5 tracking code is not visible

    @seo @dimensions @d5
    Scenario: Dimension5 tracking code is not displayed on a Funeral Directors location page if the user is not logged in
        Given I am on the "london" page
        When I check the page source "body"
        Then The dimension5 tracking code is not visible

    @seo @dimensions @d5
    Scenario: Dimension5 tracking code is not displayed on a Business Profile page if the user is not logged in
        Given I am on the "london" page
        And I select the top funeral director page that is listed
        When I check the page source "body"
        Then The dimension5 tracking code is not visible

    @seo @dimensions @d5
    Scenario: Dimension5 tracking code is not displayed on an Obituary page if the user is not logged in
        Given I am on the obituaries page for Exeter
        And I select the first obituary displayed
        When I check the page source "body"
        Then The dimension5 tracking code is not visible

    @seo @dimensions @d5
    Scenario: Dimension5 tracking code is displayed on the Funeral Zone homepage if the user is logged in
        Given I am logged in as admin
        And I am on the Funeral Zone homepage
        When I check the page source "body"
        Then The "dimension5" tracking code is visible

    @seo @dimensions @d5
    Scenario: Dimension5 tracking code is displayed on the Obituaries page if the user is logged in
        Given I am logged in as admin
        And I am on the "obituaries" page
        When I check the page source "body"
        Then The "dimension5" tracking code is visible

    @seo @dimensions @d5
    Scenario: Dimension5 tracking code is displayed on the Funeral Wishes page if the user is logged in
        Given I am logged in as admin
        And I am on the "funeral wishes" page
        When I check the page source "body"
        Then The "dimension5" tracking code is visible

    @seo @dimensions @d5
    Scenario: Dimension5 tracking code is displayed on the Funeral Plans page if the user is logged in
        Given I am logged in as admin
        And I am on the "funeral plans" page
        When I check the page source "body"
        Then The "dimension5" tracking code is visible

    @seo @dimensions @d5
    Scenario: Dimension5 tracking code is displayed on the Blog page if the user is logged in
        Given I am logged in as admin
        And I am on the "blog" page
        When I check the page source "body"
        Then The "dimension5" tracking code is visible

    @seo @dimensions @d5
    Scenario: Dimension5 tracking code is displayed on the About page if the user is logged in
        Given I am logged in as admin
        And I am on the "about" page
        When I check the page source "body"
        Then The "dimension5" tracking code is visible

    @seo @dimensions @d5
    Scenario: Dimension5 tracking code is displayed on the Contact page if the user is logged in
        Given I am logged in as admin
        And I am on the "contact" page
        When I check the page source "body"
        Then The "dimension5" tracking code is visible

    @seo @dimensions @d5
    Scenario: Dimension5 tracking code is displayed on the Features page if the user is logged in
        Given I am logged in as admin
        And I am on the "features" page
        When I check the page source "body"
        Then The "dimension5" tracking code is visible

    @seo @dimensions @d5
    Scenario: Dimension5 tracking code is displayed on the Directory page if the user is logged in
        Given I am logged in as admin
        And I am on the "directory" page
        When I check the page source "body"
        Then The "dimension5" tracking code is visible

    @seo @dimensions @d5
    Scenario: Dimension5 tracking code is displayed on the Funeral Directors A-Z page if the user is logged in
        Given I am logged in as admin
        And I am on the "funeral directors a-z" page
        When I check the page source "body"
        Then The "dimension5" tracking code is visible

    @seo @dimensions @d5
    Scenario: Dimension5 tracking code is displayed on the Privacy Policy page if the user is logged in
        Given I am logged in as admin
        And I am on the "privacy policy" page
        When I check the page source "body"
        Then The "dimension5" tracking code is visible

    @seo @dimensions @d5
    Scenario: Dimension5 tracking code is displayed on the Terms & Conditions page if the user is logged in
        Given I am logged in as admin
        And I am on the "terms & conditions" page
        When I check the page source "body"
        Then The "dimension5" tracking code is visible

    @seo @dimensions @d5
    Scenario: Dimension5 tracking code is displayed on the Sitemap page if the user if logged in
        Given I am logged in as admin
        And I am on the "sitemap" page
        When I check the page source "body"
        Then The "dimension5" tracking code is visible

    @seo @dimensions @d5
    Scenario: Dimension5 tracking code is displayed on a Funeral Directors location page if the user is logged in
        Given I am logged in as admin
        And I am on the "london" page
        When I check the page source "body"
        Then The "dimension5" tracking code is visible

    @seo @dimensions @d5
    Scenario: Dimension5 tracking code is displayed on a Business Profile page if the user is logged in
        Given I am logged in as admin
        And I am on the "london" page
        And I select the top funeral director page that is listed
        When I check the page source "body"
        Then The "dimension5" tracking code is visible

    @seo @dimensions @d5
    Scenario: Dimension5 tracking code is displayed on an Obituary page if the user is logged in
        Given I am logged in as admin
        And I am on the obituaries page for Exeter
        And I select the first obituary displayed
        When I check the page source "body"
        Then The "dimension5" tracking code is visible
