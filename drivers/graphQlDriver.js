const GraphQlRequest = require('graphql-request')

module.exports = class {
  constructor (endpointUrl, accessToken) {
    this.client = null

    this.config = {
      endpointUrl: endpointUrl,
      accessToken: accessToken
    }
  }

  init () {
    this.client = new GraphQlRequest.GraphQLClient(
      this.config.endpointUrl,
      {
        headers: {
          'X-Access-Token': this.config.accessToken,
          'X-Display-Errors': 1
        }
      }
    )
  }

  call (query, variables) {
    return this.client.request(query, variables)
  }

  quit () {
    // nothing to do
  }
}
