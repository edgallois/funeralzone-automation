const fs = require('fs')
const rimraf = require('rimraf')

let archiveRoot = process.cwd() + '/archive/'
let archiveFolder = process.cwd() + '/archive/FuneralZone_Results_'
let featuresArchive = archiveFolder + '/features/'
let jsonFiles = process.cwd() + '/jsonfiles'

// Check to see if the archive folder exists, and if not, create it
if (!fs.existsSync(archiveRoot)) {
  fs.mkdirSync(archiveRoot)
}

// Clear the old JSON files
if (fs.existsSync(jsonFiles)) {
  rimraf.sync(jsonFiles)
}

// If an older test was aborted, just nuke the folder
if (fs.existsSync(archiveFolder)) {
  rimraf.sync(archiveFolder)
}

// Create the archive folders required
fs.mkdirSync(archiveFolder)
fs.mkdirSync(featuresArchive)
fs.mkdirSync(process.cwd() + '/jsonfiles')

// Previous runs to be removed
let enrichedJson = process.cwd() + '/results/enriched-output.json'
let mergedJson = process.cwd() + '/results/merged-output.json'
let browserFile = process.cwd() + '/browserDetails.js'

if (fs.existsSync(enrichedJson)) {
  fs.unlinkSync(enrichedJson)
}

if (fs.existsSync(mergedJson)) {
  fs.unlinkSync(mergedJson)
}

if (fs.existsSync(browserFile)) {
  fs.unlinkSync(browserFile)
}

process.exit()
